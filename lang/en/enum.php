<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Enum value Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during return enum value
    */

    'fixed' => 'fixed',
    'variable' => 'variable',

    'admin' =>  'admin',
    'customer' => 'customer',

    'open' => 'open',
    'close' => 'close'
];
