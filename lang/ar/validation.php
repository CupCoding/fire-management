<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => ':attribute يجب قبول',
    'accepted_if' => ' يجب قبول :attribute عندما :other هو :value.',
    'active_url' => ':attribute ليست عنوان رابط صالح.',
    'after' => 'يجب أن تكون :attribute :date. بعد التاريخ.',
    'after_or_equal' => 'يجب أن تكون :attribute تاريخًا بعد أو :date',
    'alpha' => 'يجب أن تحتوي :attribute على رسائل فقط.',
    'alpha_dash' => 'يجب أن تحتوي :attribute فقط على رسائل وأرقام وشرطات وترسيح.',
    'alpha_num' => 'يجب أن تحتوي :attribute على رسائل وأرقام فقط.',
    'array' => 'يجب أن تكون :attribute مصفوفة.',
    'ascii' => 'يجب أن تحتوي :attribute فقط على أحرف ورموز أبجدية واحدة رقمية.',
    'before' => 'يجب أن تكون :attribute :date سابقًا التاريخ.',
    'before_or_equal' => 'يجب أن تكون :attribute تاريخًا قبل أو :date',
    'between' => [
        'array' => 'يجب ان يكون :attribute بين :min و :max العناصر.',
        'file' => 'يجب ان تكون :attribute بين :min و :max العناصر ',
        'numeric' => 'يجب ان تكون :attribute بين :min و :max',
        'string' => 'يجب ان تكون :attribute بين :min و:اقصى المحارف',
    ],
    'boolean' => 'يجب أن يكون حقل :attribute صحيحًا أو خطأ.',
    'confirmed' => ': تأكيد :attribute لا يتطابق.',
    'current_password' => 'كلمة المرور غير صحيحة.',
    'date' => ':attribute ليست تاريخ صالح.',
    'date_equals' => 'يجب أن يكون :attribute تاريخًا :date.',
    'date_format' => ':attribute لا تتطابق مع :format.',
    'decimal' => 'يجب أن يكون :attribute :decimal عشرية.',
    'declined' => 'يجب رفض :attribute.',
    'declined_if' => 'يجب رفض: :attribute عندما: :other :value. القيمة',
    'different' => ':attribute :other الأخرى يجب أن تكون مختلفة',
    'digits' => 'يجب أن تكون :attribute :digits',
    'digits_between' => 'يجب ان تكون :attribute بين :min و :max محرف رقمي',
    'dimensions' => ':attribute لها أبعاد صورة غير صالحة.',
    'distinct' => 'حقل :attribute له قيمة مكررة.',
    'doesnt_end_with' => 'قد لا تنتهي :attribute :values. أحد ما يلي',
    'doesnt_start_with' => 'قد لا تبدأ :attribute بأحد ما يلي:values',
    'email' => 'يجب أن تكون :attribute عنوان بريد إلكتروني صالح.',
    'ends_with' => 'يجب أن تنتهي :attribute بأحد ما يلي :values.',
    'enum' => 'المحدد:attribute غير صالحة',
    'exists' => 'المحدد:attribute غير صالحة',
    'file' => 'يجب أن تكون :attribute ملفًا.',
    'filled' => 'يجب أن يكون لحقل :attribute قيمة.',
    'gt' => [
        'array' => 'يجب أن تحتوي :attribute على أكثر :values عناصر',
        'file' => 'يجب أن تكون :attribute أكبر :values. قيمة كيلوبايت.',
        'numeric' => 'يجب أن تكون :attribute أكبر :values',
        'string' => 'يجب أن تكون :attribute أكبر :values أحرف',
    ],
    'gte' => [
        'array' => 'يجب أن يكون لدى :attribute: عناصر :value أو أكثر.',
        'file' => 'يجب أن تكون :attribute أكبر من أو :value كيلوايت.',
        'numeric' => 'يجب أن تكون :attribute أكبر من أو تساوي: :value.',
        'string' => 'يجب أن تكون :attribute أكبر من أو تساوي: أحرف :value.',
    ],
    'image' => 'يجب أن تكون :attribute صورة.',
    'in' => 'المحدد: :attribute غير صالحة.',
    'in_array' => ': حقل :attribute غير موجود :other',
    'integer' => 'يجب أن تكون :attribute عددًا صحيحًا.',
    'ip' => 'يجب أن تكون :attribute عنوان IP صالحًا.',
    'ipv4' => 'يجب أن تكون :attribute عنوان IPv4 صالح.',
    'ipv6' => 'يجب أن تكون :attribute عنوان IPv6 صالح.',
    'json' => 'يجب أن تكون :attribute سلسلة JSON صالحة.',
    'lowercase' => 'يجب أن تكون :attribute صغيرة.',
    'lt' => [
        'array' => 'يجب أن تحتوي :attribute على أقل من: عناصر :value.',
        'file' => 'يجب أن تكون :attribute أقل من:value الكيلوغرام.',
        'numeric' => 'يجب أن تكون :attribute أقل من: :value.',
        'string' => 'يجب أن تكون :attribute أقل من: أحرف :value.',
    ],
    'lte' => [
        'array' => 'يجب ألا تحتوي :attribute على أكثر من: عناصر :value.',
        'file' => 'يجب أن تكون :attribute أقل من أو تساوي:value كيلو بايت.',
        'numeric' => 'يجب أن تكون :attribute أقل من أو تساوي: :value.',
        'string' => 'يجب أن تكون :attribute أقل من أو تساوي: أحرف :value.',
    ],
    'mac_address' => 'يجب أن تكون :attribute عنوان MAC صالح.',
    'max' => [
        'array' => 'يجب ألا تحتوي :attribute على أكثر من:max.',
        'file' => 'يجب ألا تكون :attribute من: الحد :max كيلوبايت.',
        'numeric' => 'يجب ألا تكون :attribute من:max.',
        'string' => 'يجب ألا تكون :attribute من:max أحرف.',
    ],
    'max_digits' => 'يجب ألا تحتوي :attribute على أكثر من: :max أرقام.',
    'mimes' => 'يجب أن تكون :attribute ملف من النوع :values',
    'mimetypes' => 'يجب أن تكون :attribute ملف من النوع :values',
    'min' => [
        'array' => 'يجب أن يكون :attribute على :min عناصر مصفوفة.',
        'file' => 'يجب أن تكون :attribute على :min عنصر كيلوبايت.',
        'numeric' => 'يجب أن تكون :attribute على :min اصغر.',
        'string' => 'يجب أن تكون :attribute على :min أحرف صغيرة.',
    ],
    'min_digits' => 'يجب أن يكون :attribute على :min أرقام',
    'multiple_of' => 'يجب أن تكون :attribute مضاعفة :value.',
    'not_in' => 'المحدد:attribute غير صالحة.',
    'not_regex' => 'تنسيق :attribute غير صالح.',
    'numeric' => 'يجب أن تكون :attribute رقمًا.',
    'password' => [
        'letters' => 'يجب أن تحتوي :attribute على حرف واحد على الأقل.',
        'mixed' => 'يجب أن تحتوي :attribute على أحرف كبيرة واحدة على الأقل وحرف صغيرة واحدة.',
        'numbers' => 'يجب أن تحتوي :attribute على رقم واحد على الأقل.',
        'symbols' => 'يجب أن تحتوي :attribute على رمز واحد على الأقل.',
        'uncompromised' => 'المعطى ظهرت :attribute في تسرب البيانات. الرجاء اختيار مجموعة مختلفة: :attribute.',
    ],
    'present' => 'يجب أن يكون حقل :attribute موجودًا.',
    'prohibited' => 'حقل :attribute محظور.',
    'prohibited_if' => 'حقل :attribute محظور عندما :other هو :value.',
    'prohibited_unless' => 'حقل :attribute محظور ما لم: :other هو في :values.',
    'prohibits' => 'حقل :attribute :other من الوجود.',
    'regex' => ': تنسيق :attribute غير صالح.',
    'required' => 'مطلوب حقل :attribute.',
    'required_array_keys' => 'يجب أن يحتوي حقل :attribute على إدخالات :values.',
    'required_if' => 'حقل :attribute مطلوب عندما :other هو :value.',
    'required_if_accepted' => 'حقل :attribute مطلوب عندما يتم قبول :other.',
    'required_unless' => 'مطلوب حقل :attribute ما لم يكن :other :values',
    'required_with' => 'حقل :attribute مطلوب عندما :values موجودة.',
    'required_with_all' => 'حقل :attribute مطلوب عندما: :values موجودة.',
    'required_without' => 'حقل :attribute مطلوب عندما :values غير موجودة.',
    'required_without_all' => 'حقل :attribute مطلوب عندما لا يوجد أي من: :values موجودة.',
    'same' => ':attribute و :other يجب أن يتطابق.',
    'size' => [
        'array' => 'يجب أن تحتوي :attribute على: عناصر :size.',
        'file' => 'يجب أن تكون :attribute: :size كيلوبايت.',
        'numeric' => 'يجب أن تكون :attribute :size.',
        'string' => 'يجب أن تكون :attribute: أحرف :size.',
    ],
    'starts_with' => 'يجب أن تبدأ :attribute بواحدة مما يلي :values.',
    'string' => 'يجب أن تكون :attribute سلسلة.',
    'timezone' => 'يجب أن تكون :attribute منطقة زمنية صالحة.',
    'unique' => ':attribute قد أخذت بالفعل.',
    'uploaded' => 'فشل :attribute في التحميل.',
    'uppercase' => 'يجب أن تكون :attribute كبيرة.',
    'url' => 'يجب أن تكون :attribute عنوان URL صالحًا.',
    'ulid' => 'يجب أن تكون :attribute ulid صالحة.',
    'uuid' => 'يجب أن تكون :attribute uuid صالحة.',
    'not_found' => 'العنصر غير موجود!',


    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'رسالة مخصصة',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [],
];
