<?php

return [
   /*
   |--------------------------------------------------------------------------
   | Authentication Language Lines
   |--------------------------------------------------------------------------
   |
   | The following language lines are used during authentication for various
   | messages that we need to display to the user. You are free to modify
   | these language lines according to your application's requirements.
   |
   */

   'failed' => 'ايميل و الباسوورد غير صحيحية ',
   'password' => 'كلمة المرور المقدمة غير صحيحة.',
   'throttle' => 'الكثير من محاولات تسجيل الدخول. يرجى المحاولة مرة أخرى في:seconds.',
   'unauthenticated' => 'مستخدم غير مصادق يرجى تسجيل الدخول',
   'email_password_invalid' => 'ايميل او الباسورد المدخل غير صحيح.',
   'otp_not_correct' => 'الرمز المدخل غير صحيح.',
   'otp_expire' => 'الرمز المدخل منتهي الصلاحية.'
];