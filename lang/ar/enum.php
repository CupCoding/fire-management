<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Enum value Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during return enum value
    */

    'fixed' => 'ثابتة',
    'variable' => 'متغيرة',

    'admin' =>  'مسؤول النظام',
    'customer' => 'المستخدم',

    'open' => 'مفتوح',
    'close' => 'مغلق'
];
