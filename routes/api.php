<?php

use App\Http\Controllers\Admin\Auth\AuthController as AdminAuthController;
use App\Http\Controllers\Mobile\Auth\AuthController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//admin login
Route::controller(AdminAuthController::class)->group(function () {
    Route::post('admin/login', 'login');
    Route::post('admin/refresh-token', 'refreshToken')->middleware('refresh_token_admin');
});

//Mobile login
Route::controller(AuthController::class)->group(function () {
    Route::post('/otp/login', 'generateOtpCode');
    Route::post('/otp/verification', 'verifyOtpLogin');
    Route::post('/refresh-token', 'refreshToken')->middleware('refresh_token_mobile');
});


Route::get('cache_clear', function () {
    Artisan::call('cache:clear');
    return 'Done';
});
