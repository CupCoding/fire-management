<?php

use App\Http\Controllers\Admin\{
    CenterController,
    UserController,
    RoleController,
    CarController
};
use App\Http\Controllers\Admin\Auth\AuthController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::get('profile', [AuthController::class, 'profile']);

Route::apiResource('users', UserController::class);
Route::apiResource('roles', RoleController::class);
Route::apiResource('centers', CenterController::class);
Route::apiResource('cars', CarController::class);

