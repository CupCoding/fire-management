<?php

use App\Http\Controllers\Mobile\Auth\ProfileController;

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::get('profile', [ProfileController::class, 'profile']);
Route::post('update-profile', [ProfileController::class, 'update']);
Route::post('change-password', [ProfileController::class, 'changePassword']);
Route::get('check-user', [ProfileController::class, 'checkUser']);


