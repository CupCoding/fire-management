import * as React from "react";
import { Link, Navigate, createBrowserRouter } from "react-router-dom";
import { FullPageErrorFallback } from "@/components/lib";

import LogoOnlyLayout from "./layouts/LogoOnlyLayout";
import DashboardLayout from "./layouts/dashboard";
import NotFound from "./screens/Page404";

import Categories from "./screens/Categories";
import AddCategory from "./screens/Categories/Add";
import EditCategory from "./screens/Categories/EditCategory";

import Users from "./screens/Users";
import AddUsers from "./screens/Users/Add";
import EditUser from "./screens/Users/EditUser";

import Specifications from "./screens/Specification";
import AddSpecification from "./screens/Specification/Add";
import EditSpecification from "./screens/Specification/Edit";
import ShowSpecification from "./screens/Specification/Show";

import Brands from "./screens/Brands";
import AddBrand from "./screens/Brands/Add";
import EditBrand from "./screens/Brands/Edit";

import Countries from "./screens/Countries";
import AddCountry from "./screens/Countries/Add";
import EditCountry from "./screens/Countries/Edit";

import Currencies from "./screens/Currency";
import AddCurrency from "./screens/Currency/Add";
import EditCurrency from "./screens/Currency/Edit";

import { defaultLang, supportedLanguages } from "./constants";

import UserShow from "./screens/Users/Show";
import AddAddress from './screens/Users/Show/Address/Add/index';
import EditAddress from './screens/Users/Show/Address/Edit/index';

import Templates from './screens/SpecificationTemplate'
import AddTemplate from './screens/SpecificationTemplate/Add'
import EditTemplate from './screens/SpecificationTemplate/Edit'

import Products from './screens/Products'
import AddProduct from './screens/Products/Add'
import EditProduct from './screens/Products/Edit'
import ShowProduct from './screens/Products/Show'

import FAQs from './screens/FAQs'
import AddFAQ from './screens/FAQs/Add'
import EditFAQ from './screens/FAQs/Edit'

import Contacts from './screens/Contact-us'
import ShowContact from './screens/Contact-us/show'

import Reviews from './screens/Reviews'
import ShowReview from './screens/Reviews/show'

import Roles from './screens/Roles'
import AddRole from './screens/Roles/Add'
import EditRole from './screens/Roles/Edit'

import AddProductSpecification from "./screens/Products/Show/Specification/Add";

import Test from "./screens/Test";
// ----------------------------------------------------------------------
const checkLangBeforeLoading = ({ params }) => {
    const lang = params?.lang ?? defaultLang;
    const isSupported = supportedLanguages.includes(lang);
    if (!isSupported) {
        const currLangRout = `/${lang}`;
        const ReplacedRout = `/${defaultLang}`;
        window.location.replace(
            window.location.pathname.replace(currLangRout, ReplacedRout)
        );
    }
    return isSupported;
};
const useRouter = () => {
    return createBrowserRouter(
        [
            {
                path: "/",
                element: <LogoOnlyLayout />,
                children: [
                    {
                        index: true,
                        element: <Navigate to={`${defaultLang}/`} replace />,
                    },
                    { path: "/404", element: <NotFound /> },
                    {
                        path: "*",
                        element: <Navigate to="404" replace />,
                    },
                ],
            },
            {
                path: "/:lang",
                element: <DashboardLayout />,
                errorElement: <FullPageErrorFallback />,
                loader: checkLangBeforeLoading,
                handle: {
                    crumb: () => <Link to="/">dashboard</Link>,
                },
                children: [
                    {
                        path: "users",
                        handle: {
                            crumb: () => <Link to="/users">users</Link>,
                        },
                        children: [
                            { index: true, element: <Users /> },
                            {
                                path: "add",
                                element: <AddUsers />,
                                handle: {
                                    crumb: () => (
                                        <Link to="/add">new_user</Link>
                                    ),
                                },
                            },
                            {
                                path: ":id/edit",
                                element: <EditUser />,
                                handle: {
                                    crumb: () => (
                                        <Link to="/:id/edit">edit_user</Link>
                                    ),
                                },
                            },
                            {
                                path: ':id',
                                element:<UserShow />,
                                handle: {
                                    crumb: () => (
                                        <Link to="/:id">user_details</Link>
                                    ),
                                },
                            },
                            {
                                path:':id/address/add',
                                element:<AddAddress />,
                                handle: {
                                    crumb: () => (
                                        <Link to="/:id/address/add">address</Link>
                                    ),
                                },
                            },
                            {
                                path:':id/address/add',
                                element:<AddAddress />,
                                handle: {
                                    crumb: () => (
                                        <Link to="/:id/address/:addressId/add">add_address</Link>
                                    ),
                                },
                            },

                            {
                                path:':id/address/:addressId/edit',
                                element:<EditAddress />,
                                handle: {
                                    crumb: () => (
                                        <Link to="/:id/address/:addressId/edit">address</Link>
                                    ),
                                },
                            }
                        ],
                    },
                    {
                        path:'brands',
                        handle: {
                            crumb: () => <Link to="/brands">brands</Link>,
                        },
                        children: [
                            { index: true, element: <Brands /> },
                            {
                                path: "add",
                                element: <AddBrand />,
                                handle: {
                                    crumb: () => (
                                        <Link to="/add">new_brand</Link>
                                    ),
                                },
                            },
                            {
                                path: ":id/edit",
                                element: <EditBrand />,
                                handle: {
                                    crumb: () => (
                                        <Link to="/:id/edit">edit_brand</Link>
                                    ),
                                },
                            },
                        ],
                    },
                    {
                        path:'categories',
                        handle: {
                            crumb: () => <Link to="/users">users</Link>,
                        },
                        children:[
                           {index:true,element:<Categories/>},
                           {
                            path: "add",
                            element: <AddCategory />,
                            handle: {
                                crumb: () => (
                                    <Link to="/add">new_category</Link>
                                ),
                            },
                        },
                        {
                            path: ":id/edit",
                            element: <EditCategory />,
                            handle: {
                                crumb: () => (
                                    <Link to="/:id/edit">edit_category</Link>
                                ),
                            },
                        },
                        ]
                    },
                    {
                        path: "specifications",
                        handle: {
                            crumb: () => <Link to="/specifications">specifications</Link>,
                        },
                        children: [
                            { index: true, element: <Specifications /> },
                            {
                                path: "add",
                                element: <AddSpecification />,
                                handle: {
                                    crumb: () => (
                                        <Link to="/add">new_specification</Link>
                                    ),
                                },
                            },
                            {
                                path: ":id/edit",
                                element: <EditSpecification />,
                                handle: {
                                    crumb: () => (
                                        <Link to="/:id/edit">edit_specification</Link>
                                    ),
                                },
                            },
                            {
                                path: ":id",
                                element: <ShowSpecification />,
                                handle: {
                                    crumb: () => (
                                        <Link to="/:id">show_specification</Link>
                                    ),
                                },
                            },
                        ],
                    },
                    {
                        path:'countries',
                        handle: {
                            crumb: () => <Link to="/countries">countries</Link>,
                        },
                        children: [
                            { index: true, element: <Countries /> },
                            {
                                path: "add",
                                element: <AddCountry />,
                                handle: {
                                    crumb: () => (
                                        <Link to="/add">new_country</Link>
                                    ),
                                },
                            },
                            {
                                path: ":id/edit",
                                element: <EditCountry />,
                                handle: {
                                    crumb: () => (
                                        <Link to="/:id/edit">edit_country</Link>
                                    ),
                                },
                            },
                        ],
                    },
                    {
                        path:'currencies',
                        handle: {
                            crumb: () => <Link to="/currencies">currency</Link>,
                        },
                        children: [
                            { index: true, element: <Currencies /> },
                            {
                                path: "add",
                                element: <AddCurrency />,
                                handle: {
                                    crumb: () => (
                                        <Link to="/add">new_currency</Link>
                                    ),
                                },
                            },
                            {
                                path: ":id/edit",
                                element: <EditCurrency />,
                                handle: {
                                    crumb: () => (
                                        <Link to="/:id/edit">edit_currency</Link>
                                    ),
                                },
                            },
                        ],
                    },
                    {
                        path:'templates',
                        handle: {
                            crumb: () => <Link to="/templates">template</Link>,
                        },
                        children: [
                            { index: true, element: <Templates /> },
                            {
                                path: "add",
                                element: <AddTemplate />,
                                handle: {
                                    crumb: () => (
                                        <Link to="/add">new_template</Link>
                                    ),
                                },
                            },
                            {
                                path: ":id/edit",
                                element: <EditTemplate />,
                                handle: {
                                    crumb: () => (
                                        <Link to="/:id/edit">edit_template</Link>
                                    ),
                                },
                            },
                        ],
                    },
                    {
                        path:'products',
                        handle: {
                            crumb: () => <Link to="/products">products</Link>,
                        },
                        children:[
                            {
                                index:true,
                                element:<Products/>
                            },
                            {
                                path:'add',
                                handle:{
                                    crumb:() => <Link to=":productId/add">products_add</Link>
                                },
                                element:<AddProduct />

                            },
                            {
                                path:':productId/add',
                                handle:{
                                    crumb:() => <Link to=":productId/add">products_details</Link>
                                },
                                element:<AddProductSpecification />
                            },
                            {
                                path:':productId/edit',
                                handle:{
                                    crumb:() => <Link to=":productId/edit">products_edit</Link>
                                },
                                element:<EditProduct />
                            },
                            {
                                path:':productId/show',
                                handle:{
                                    crumb:() => <Link to=":productId/show">product_details</Link>
                                },
                                element:<ShowProduct/>
                            }
                        ]
                    },
                    {
                        path:'faqs',
                        handle: {
                            crumb: () => <Link to="/faqs">faqs</Link>,
                        },
                        children: [
                            { index: true, element: <FAQs /> },
                            {
                                path: "add",
                                element: <AddFAQ />,
                                handle: {
                                    crumb: () => (
                                        <Link to="/add">new_faq</Link>
                                    ),
                                },
                            },
                            {
                                path: ":id/edit",
                                element: <EditFAQ />,
                                handle: {
                                    crumb: () => (
                                        <Link to="/:id/edit">edit_faq</Link>
                                    ),
                                },
                            },
                        ],
                    },
                    {
                        path:"contact-us",
                        handle: {
                            crumb: () => <Link to="/contact-us">contact-us</Link>,
                        },
                        children:[
                            { index: true, element: <Contacts />},
                            {
                                path: ":id/show",
                                element: <ShowContact />,
                                handle: {
                                    crumb: () => (
                                        <Link to="/:id/show">contact_request</Link>
                                    ),
                                },
                            },

                        ]
                    },
                    {
                        path:"reviews",
                        handle: {
                        crumb: () => <Link to="/reviews">reviews</Link>,
                        },
                        children:[
                            { index: true, element: <Reviews />},
                            {
                                path: ":id/show",
                                element: <ShowReview />,
                                handle: {
                                    crumb: () => (
                                        <Link to="/:id/show">review_details</Link>
                                    ),
                                },
                            },

                        ]
                    },
                    {
                        path:'roles',
                        handle: {
                            crumb: () => <Link to="/roles">roles</Link>,
                        },
                        children: [
                            { index: true, element: <Roles /> },
                            {
                                path: "add",
                                element: <AddRole />,
                                handle: {
                                    crumb: () => (
                                        <Link to="/add">new_role</Link>
                                    ),
                                },
                            },
                            {
                                path: ":id/edit",
                                element: <EditRole />,
                                handle: {
                                    crumb: () => (
                                        <Link to="/:id/edit">edit_role</Link>
                                    ),
                                },
                            },
                        ],
                    },
                    {
                        path:'test',
                        element:<Test/>
                    }
                ],
            },
            {
                path: "/lang",
                element: <>LANG</>,
            },
        ],
        { basename: "/dashboard" }
    );
};

export default useRouter;
