const localStorageKeyToken = "__auth_provider_token__";
const localStorageKeyRefreshToken = "__auth_provider_refresh_token__";
import axios from "axios";

async function getRefreshToken() {
    return {
        refresh_token:window.localStorage.getItem(localStorageKeyRefreshToken),
        token:window.localStorage.getItem(localStorageKeyToken)
    };
}

function handleUserResponse(user) {
    window.localStorage.setItem(localStorageKeyRefreshToken, user?.refresh_token);
    window.localStorage.setItem(localStorageKeyToken, user?.token);
    return {
        ...user?.admin,
        token: user.token,
        refresh_token:user.refresh_token,
    };
}

function login({ email, password }) {
    return client("login", { email, password }).then(handleUserResponse);
}

function register(user) {
    return client("register", {
        ...user,
    }).then(handleUserResponse);
}

async function logout() {
    window.localStorage.removeItem(localStorageKeyRefreshToken);
    window.localStorage.removeItem(localStorageKeyToken);
}

const authURL = `${window.location.origin}/api/admin`;

async function client(endpoint, data) {
    const config = {
        url: `${authURL}/${endpoint}`,
        method: "POST",
        data: data,
        headers: {
            "Content-Type": "application/json",
        },
    };

    return axios(config).then(async (response) => {
        if (response.status === 200) {
            return response?.data?.content;
        } else {
            return Promise.reject(data);
        }
    });
}

export { getRefreshToken, login, register, logout, localStorageKeyToken,localStorageKeyRefreshToken,handleUserResponse };
