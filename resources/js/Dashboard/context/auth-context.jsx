import * as React from "react";
import { QueryClient } from "react-query";
import * as auth from "../auth-provider";
import { client } from "../utils/api";
import { useAsync } from "@/utils/hooks";
import { FullPageSpinner, FullPageErrorFallback } from "../components/lib";
import useLang from "../hooks/useLang";
import {handleUserResponse} from '../auth-provider';

async function bootstrapAppData() {
    const queryCache = new QueryClient();
    let user = null;
    const {refresh_token,token} = await auth?.getRefreshToken();
    if (refresh_token) {
        const data = await client(`refresh-token`, {
            data:{},
            headers:{
                'refresh-token':refresh_token,
                Authorization:`Bearer ${token}`

            }});

        queryCache.setQueryData("user", data?.data?.user, {
            staleTime: 5000,
        });

        user = {
            ...data?.data?.content,
            refresh_token:data?.data?.refresh_token,
            token:data?.data?.token
        };

        handleUserResponse(user)
    }
    return user;
}

const AuthContext = React.createContext();
AuthContext.displayName = "AuthContext";

function AuthProvider(props) {
    const {
        data: user,
        status,
        error,
        isLoading,
        isIdle,
        isError,
        isSuccess,
        run,
        setData,
    } = useAsync();
    const queryCache = new QueryClient();

    React.useEffect(() => {
        const appDataPromise = bootstrapAppData();
        run(appDataPromise);
    }, [run]);

    const refreshUser = () => {
        const appDataPromise = bootstrapAppData();
        run(appDataPromise);
    };
    const login = React.useCallback(
        (form) =>
            auth.login(form).then((user) => {
                setData({ ...user });
            }),
        [setData]
    );
    const register = React.useCallback(
        (form) => auth.register(form).then((user) => setData({ ...user })),
        [setData]
    );
    const logout = React.useCallback(() => {
        auth.logout();
        queryCache.clear();
        setData(null);
    }, [setData]);

    const value = React.useMemo(
        () => ({ user, login, logout, register, refreshUser }),
        [login, logout, register, user, refreshUser]
    );

    if (isLoading || isIdle) {
        return <FullPageSpinner />;
    }

    if (isError) {
        return <FullPageErrorFallback error={error} />;
    }

    if (isSuccess) {
        return <AuthContext.Provider value={value} {...props} />;
    }

    throw new Error(`Unhandled status: ${status}`);
}

function useAuth() {
    const context = React.useContext(AuthContext);
    if (context === undefined) {
        throw new Error(`useAuth must be used within a AuthProvider`);
    }
    return context;
}

function useClient(ContentType) {
    const { user } = useAuth();
    const token = user?.token;
    const { lang } = useLang();
    const headers = {
        "Content-Type": ContentType ?? 'application/json',
        "accept-language": lang,
    };
    return React.useCallback(
        (endpoint, config) => client(endpoint, { ...config, headers, token }),
        [token, lang]
    );
}

function useAxios() {

}

export { AuthProvider, useAuth, useClient };
