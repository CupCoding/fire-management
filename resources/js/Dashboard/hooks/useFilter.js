import { useSearchParams } from "react-router-dom"
//pagination Logic
//pass id and name
//read and translate into arabic

export function readQueryParams(guards = [], ignore = [], specialHandle = [(searchParams) => { }]) {
    let [searchParams, setSearchParams] = useSearchParams()

    let query = {
        page: searchParams.get('page') || 1,
        perPage: searchParams.get('limit') || 10,
        sorts: searchParams.get('sorts') || undefined,
        search: searchParams.get('query') || undefined
    }

    if (guards?.length > 0) {
        for (const [key, value] of searchParams.entries()) {
            if (guards.includes(key)) {
                query[key] = value
            }
        }
    }

    else {
        for (const [key, value] of searchParams.entries()) query[key] = value
    }
    return query;
}
