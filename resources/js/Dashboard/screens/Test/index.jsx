import React from "react";
import Page from "@/components/Page";
import { Container, Stack } from "@mui/material";

const Test = () => {
    return (
        <Page title="Product Specifications">
            <Container>
                <Stack flexDirection="col">
                    test-page
                </Stack>
            </Container>
        </Page>
    );
};

export default Test;
