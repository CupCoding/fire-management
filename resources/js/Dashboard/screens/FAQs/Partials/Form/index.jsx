import CustomInput from "@/components/Form/components/CustomInput";
import { FullPageSpinner } from "@/components/lib";
import { useClient } from "@/context/auth-context";
import { getErrorsFromResponse } from "@/utils/fromHelper";
import { successWithCustomMessage } from "@/utils/notifications";
import { getRouteWithLang } from "@/utils/routesHelpers";
import { yupResolver } from "@hookform/resolvers/yup";
import { Stack } from "@mui/material";
import React, { useEffect } from "react";
import { useForm } from "react-hook-form";
import { useMutation, useQuery, useQueryClient } from "react-query";
import { useNavigate, useParams } from "react-router-dom";
import * as Yup from "yup";
import SubmitLayout from '@/components/SubmitLayout';
import ErrorAlert from "@/components/ErrorAlert";
import { readTranslationFromResponse } from "../../../../utils/data-managment";

export default function Form() {
    const { id } = useParams();
    const client = useClient();
    const queryClient = useQueryClient();
    const navigate = useNavigate();
    const faqsRoute = getRouteWithLang("/faqs");
    const [backendErrors, setBackendErrors] = React.useState([]);

    const Schema = Yup.object().shape({
        question_ar: Yup.string().required("question_ar_is_required"),
        question_en: Yup.string().required("question_en_is_required"),
        answer_ar: Yup.string().required("answer_ar_is_required"),
        answer_en: Yup.string().required("answer_en_is_required"),
        type: Yup.string().required("type_is_required"),
    });

    const { isLoading: fetchLoading, data: faq } = useQuery({
        queryKey: `faq_${id}`,
        queryFn: () => client(`faqs/${id}`).then((data) => data.data),
        enabled: id !== undefined,
    });

    const {
        control,
        handleSubmit,
        reset,
        formState: { errors, isDirty },
    } = useForm({
        resolver: yupResolver(Schema),
        defaultValues: {
            question_ar: "",
            question_en: "",
            answer_ar: "",
            answer_en: "",
            type: "",
        },
    });

    useEffect(() => {
        if (faq && id !== undefined) {
            reset({
                ...readTranslationFromResponse(faq)
            })
        }
    }, [faq]);

    const {mutate, isError, isLoading} = useMutation(
        data =>
          client(`${id ? `faqs/${id}` : 'faqs'} `, {
            method: id ? 'PUT' : 'POST',
            data,
          }),
        {
          onSuccess: () => {
            queryClient.invalidateQueries('faqs')
            navigate(`${faqsRoute}`)
            reset()
            if (id) successWithCustomMessage('updated_success_msg')
            else successWithCustomMessage('added_success_msg')
          },
          onError: error => {
            let errors = getErrorsFromResponse(error)
            setBackendErrors(errors)
          },
        },
      )

    const onSubmitForm = ({
        question_ar,
        question_en,
        answer_ar,
        answer_en,
        type
    }) => {
        mutate({
            en: {
                questions: question_en,
                answer: answer_en
            },
            ar: {
                questions: question_ar,
                answer: answer_ar
            },
            type
        })
    };

    if (fetchLoading) {
        return <FullPageSpinner />;
    }

    return (
        <>
            <form
                autoComplete="off"
                noValidate
                onSubmit={handleSubmit(onSubmitForm)}
            >
                <Stack spacing={3}>
                    <ErrorAlert isError={isError} errors={backendErrors} />

                    <CustomInput
                        label="question_ar"
                        name="question_ar"
                        control={control}
                        errors={errors}
                    />
                    <CustomInput
                        label="answer_ar"
                        name="answer_ar"
                        control={control}
                        errors={errors}
                    />
                    <CustomInput
                        label="question_en"
                        name="question_en"
                        control={control}
                        errors={errors}
                    />
                    <CustomInput
                        label="answer_en"
                        name="answer_en"
                        control={control}
                        errors={errors}
                    />
                    <CustomInput
                        label="type"
                        name="type"
                        control={control}
                        errors={errors}
                    />

                </Stack>
                <SubmitLayout
                    isLoading={isLoading}
                    isDisabled={!isDirty}
                    label={id !== undefined ? 'update' : 'save'}
                    cancelAction={()=> navigate(-1)}
                />
            </form>
        </>
    );
}
