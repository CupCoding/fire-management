import React from "react";
import MoreMenu from "./Partials/MoreMenu";

export const tableColumns = [
    {
        header: "questions",
        accessorKey: "questions",
        enableSorting: false,
    },
    {
        header: "answer",
        accessorKey: "answer",
        enableSorting: false,
    },
    {
        header: "type",
        accessorKey: "type",
        enableSorting: false,
    },
    {
        header: "actions",
        accessorKey: "actions",
        cell: ({ row }) => (<MoreMenu id={row.original.id}/>),
        enableSorting: false,
    },
];
