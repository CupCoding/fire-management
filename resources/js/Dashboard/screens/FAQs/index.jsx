import * as React from "react";
import { Button, Container, Stack, Typography } from "@mui/material";
import ReactTableV2 from "@/components/ReactTableV2";
import { useMemo } from "react";
import { Link as RouterLink } from "react-router-dom";
import Iconify from "@/components/Iconify";
import Page from "@/components/Page";
import { tableColumns } from "./data";
import { useQuery } from "react-query";
import { useClient } from "@/context/auth-context";
import { FormattedMessage } from "react-intl";
import { getRouteWithLang } from "@/utils/routesHelpers";
import Breadcrumbs from "@/components/Breadcrumbs";
import queryString from "query-string";
import { readQueryParams } from '@/hooks/useFilter';
import {errorWithCustomMessage} from "@/utils/notifications";
export default function FAQs() {
    const columns = useMemo(() => tableColumns, []);
    const client = useClient();
    const fetchDataOptions = readQueryParams();
    const { data: faqs, isLoading } = useQuery(
        ["faqs", fetchDataOptions],
        () => client(`faqs?${queryString.stringify(fetchDataOptions)}`),
        {
            keepPreviousData: true,
            onError: () => {
                errorWithCustomMessage("failed_with_reload_msg");
            },
        }
    );

    return (
        <Page title="All FAQs">
            <Container>
                <Stack
                    direction="row"
                    alignItems="center"
                    justifyContent="space-between"
                    mb={1}
                >
                    <Typography variant="h4">
                        <FormattedMessage id="faqs" />
                    </Typography>

                    <Button
                        variant="contained"
                        component={RouterLink}
                        to={getRouteWithLang("/faqs/add")}
                        startIcon={<Iconify icon="eva:plus-fill" />}
                    >
                        <FormattedMessage id="new_faq" />
                    </Button>
                </Stack>
                <Breadcrumbs />
                <ReactTableV2
                    columns={columns}
                    tableData={faqs}
                    isToolbar={false}
                    isLoading={isLoading}
                />
            </Container>
        </Page>
    );
}
