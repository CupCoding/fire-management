import React from "react";
import { useQuery, useMutation, useQueryClient } from "react-query";
import { useClient } from "@/context/auth-context";
import queryString from "query-string";
import ReactTableV2 from "@/components/ReactTableV2";
import Iconify from "@/components/Iconify";
import { StyledMenuItem } from "@/components/StyledComponents";
import { FormattedMessage } from "react-intl";
import ConfirmationModal from "@/components/ConfirmationModal";
import {
    errorWithCustomMessage,
    successWithCustomMessage,
} from "@/utils/notifications";
import { Stack, MenuItem } from "@mui/material";
import { readQueryParams } from "@/hooks/useFilter";
import { useParams } from "react-router-dom";

const index = ({ OpenTheModal, setEditableId }) => {
    const { id } = useParams();
    const client = useClient();
    const [openConfirmation, setOpenConfirmation] = React.useState(false);
    const queryClient = useQueryClient();
    const [removeId, setRemoveId] = React.useState(undefined);
    const fetchDataOptions = readQueryParams();
    fetchDataOptions.specification_id = id;
    const { data: specification_values, isLoading } = useQuery(
        ["specification-values", fetchDataOptions],
        () =>
            client(
                `specification-values?${queryString.stringify(
                    fetchDataOptions
                )}`
            ),
        {
            keepPreviousData: true,
            onError: () => {
                errorWithCustomMessage("failed_with_reload_msg");
            },
        }
    );

    const { mutate: handleRemoveClick, isLoading: removeLoading } = useMutation(
        ({ id }) => client(`specification-values/${id}`, { method: "DELETE" }),
        {
            onSuccess: () => {
                queryClient.invalidateQueries("specification-values");
                successWithCustomMessage("delete_success_msg");
            },
            onError: (data) => {
                if (!data.success) {
                    errorWithCustomMessage(data?.error);
                }
                setIsOpen(false);
            },
        }
    );

    return (
        <>
            {openConfirmation && (
                <ConfirmationModal
                    isLoading={removeLoading}
                    onSave={() => {
                        handleRemoveClick({ id: removeId });
                        setOpenConfirmation(false);
                    }}
                    closeConfirmation={() => {
                        setOpenConfirmation(false);
                    }}
                    message={<FormattedMessage id="delete" />}
                    confirmation={openConfirmation}
                />
            )}
            <ReactTableV2
                columns={[
                    {
                        header: "value",
                        accessorKey: "value",
                    },
                    {
                        header: "actions",
                        cell: ({ row }) => (
                            <Stack flexDirection="row">
                                <MenuItem
                                    onClick={() => {
                                        setEditableId(row.original.id);
                                        OpenTheModal();
                                    }}
                                >
                                    <Iconify
                                        icon="eva:edit-fill"
                                        width={24}
                                        height={24}
                                    />
                                </MenuItem>

                                <StyledMenuItem
                                    onClick={() => {
                                        setRemoveId(row.original.id);
                                        setOpenConfirmation(true);
                                    }}
                                >
                                    <Iconify
                                        icon="eva:trash-2-outline"
                                        width={24}
                                        height={24}
                                    />
                                </StyledMenuItem>
                            </Stack>
                        ),
                    },
                ]}
                tableData={specification_values}
                isToolbar={false}
                isLoading={isLoading}
            />
        </>
    );
};

export default index;
