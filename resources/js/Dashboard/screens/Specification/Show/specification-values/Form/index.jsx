import React from 'react';
import CustomInput from '@/components/Form/components/CustomInput';
import {FullPageSpinner} from '@/components/lib';
import {useClient} from '@/context/auth-context';
import {getErrorsFromResponse} from '@/utils/fromHelper';
import {successWithCustomMessage} from '@/utils/notifications';
import {getRouteWithLang} from '@/utils/routesHelpers';
import {yupResolver} from '@hookform/resolvers/yup';
import {LoadingButton} from '@mui/lab';
import {Alert, Stack} from '@mui/material';
import {useEffect} from 'react';
import {useForm} from 'react-hook-form';
import {FormattedMessage} from 'react-intl';
import {useMutation, useQuery, useQueryClient} from 'react-query';
import {useNavigate, useParams} from 'react-router-dom';
import * as Yup from 'yup';

const index = ({id , closeModal}) => {
    const { id:specificationId } = useParams();
    const client = useClient();
    const queryClient = useQueryClient();
    const navigate = useNavigate();
    const specificationsRoute = getRouteWithLang(`/specifications/${specificationId}`);
    const [backendErrors, setBackendErrors] = React.useState([]);

    const Schema = Yup.object().shape({
        value_ar: Yup.string().required("value_ar_is_required"),
        value_en: Yup.string().required("value_en_is_required"),
    });

    const { isLoading: fetchLoading, data: specification } = useQuery({
        queryKey: `specification-values_${id}`,
        queryFn: () => client(`specification-values/${id}`).then((data) => data),
        enabled: id !== undefined,
    });

    const {
        control,
        handleSubmit,
        reset,
        formState: { errors, isDirty },
    } = useForm({
        resolver: yupResolver(Schema),
        defaultValues: {
            value_ar: "",
            value_en: "",
        },
    });

    useEffect(() => {
        if (specification && id !== undefined) {
            let ReceivedSpecification = {}
            specification?.data?.translations.map(lang =>{
                if(lang.locale === 'ar'){
                    ReceivedSpecification.value_ar = lang.value
                }
                else{
                    ReceivedSpecification.value_en = lang.value
                }
            })
            reset({
                ...ReceivedSpecification
            })
        }
    }, [specification]);

    const {mutate, isError, isLoading} = useMutation(
        data =>
          client(`${id ? `specification-values/${id}` : 'specification-values'} `, {
            method: id ? 'PUT' : 'POST',
            data,
          }),
        {
          onSuccess: () => {
            queryClient.invalidateQueries('specification-values')
            navigate(`${specificationsRoute}`)
            reset()
            if (id) successWithCustomMessage('updated_success_msg')
            else successWithCustomMessage('added_success_msg')
            closeModal()
          },
          onError: error => {
            let errors = getErrorsFromResponse(error)
            setBackendErrors(errors)
          },
        },
      )
      const onSubmitForm = ({value_en, value_ar}) => {
        mutate({
            en: {
                value: value_en,
            },
            ar: {
                value: value_ar,
            },
            specification_id: specificationId
        })
      }

    if (fetchLoading) {
        return <FullPageSpinner />;
    }

    return (
        <>
            <form
                autoComplete="off"
                noValidate
                onSubmit={handleSubmit(onSubmitForm)}
            >
                <Stack spacing={3}>
                    {isError ? (
                        <Alert severity="error">
                            {backendErrors.map(
                                ({ filedName, errorMessage }) => {
                                    return (
                                        <div>
                                            <FormattedMessage id={filedName} />
                                            <p>{errorMessage}</p>
                                        </div>
                                    );
                                }
                            )}
                        </Alert>
                    ) : null}

                    <CustomInput
                        label="value_ar"
                        name="value_ar"
                        control={control}
                        errors={errors}
                    />
                    <CustomInput
                        label="value_en"
                        name="value_en"
                        control={control}
                        errors={errors}
                    />
                </Stack>
                <Stack
                    direction="row"
                    alignItems="center"
                    justifyContent="flex-end"
                    sx={{ my: 2 }}
                >
                    <LoadingButton
                        onClick={closeModal}
                        size="large"
                        variant="contained"
                        sx={{ mr: 2 }}
                    >
                        <FormattedMessage id="cancel" />
                    </LoadingButton>
                    <LoadingButton
                        size="large"
                        type="submit"
                        variant="contained"
                        loading={isLoading}
                        disabled={!isDirty}
                    >
                        {id !== undefined ? (
                            <FormattedMessage id="update" />
                        ) : (
                            <FormattedMessage id="save" />
                        )}
                    </LoadingButton>
                </Stack>
            </form>
        </>
    );
}

export default index;
