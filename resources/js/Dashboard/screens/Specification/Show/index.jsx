import {Container, Stack, Typography, Button} from '@mui/material';
import Page from '@/components/Page'
import React from 'react'
import {FormattedMessage} from 'react-intl'
import Breadcrumbs from '@/components/Breadcrumbs'
import Iconify from '@/components/Iconify';
import Form from './specification-values/Form';
import SpecificationVales from './specification-values';
import {ClickAwayModal} from '@/components/StyledComponents';

export default function Show() {
    const [openValueModal,setOpenValueModal] = React.useState(false);
    const OpenTheModal = ()=> setOpenValueModal(true)
    const CloseTheModal = ()=> setOpenValueModal(false)
    const [editableId,setEditableId] = React.useState(undefined)

    return (
        <Page title="Specification">
            <Container>
                <Stack
                direction="row"
                alignItems="center"
                justifyContent="space-between"
                >
                <Typography variant="h4" gutterBottom>
                    <FormattedMessage id="specification_values" />
                </Typography>
                <Button
                    variant="contained"
                    startIcon={<Iconify icon="eva:plus-fill" />}
                    onClick={OpenTheModal}
                >
                    <FormattedMessage id="new" />
                </Button>
                </Stack>
                <Breadcrumbs />
                <SpecificationVales
                    setEditableId={setEditableId}
                    OpenTheModal={OpenTheModal}
                />

            </Container>
            {
                openValueModal && <ClickAwayModal
                    handleClose={()=> {
                        CloseTheModal()
                        setEditableId(undefined)
                    }}
                    open={openValueModal}
                >
                    <Form closeModal={()=> {
                        CloseTheModal()
                        setEditableId(undefined)
                    }} id={editableId} />
                </ClickAwayModal>
            }
        </Page>
  )
}
