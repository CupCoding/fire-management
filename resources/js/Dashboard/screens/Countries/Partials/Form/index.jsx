import CustomInput from "@/components/Form/components/CustomInput";
import { FullPageSpinner } from "@/components/lib";
import { useClient } from "@/context/auth-context";
import { getErrorsFromResponse } from "@/utils/fromHelper";
import { successWithCustomMessage } from "@/utils/notifications";
import { getRouteWithLang } from "@/utils/routesHelpers";
import { yupResolver } from "@hookform/resolvers/yup";
import { Stack } from "@mui/material";
import React, { useEffect } from "react";
import { useForm } from "react-hook-form";
import { useMutation, useQuery, useQueryClient } from "react-query";
import { useNavigate, useParams } from "react-router-dom";
import * as Yup from "yup";
import AsyncSelect from "@/components/Form/components/AsyncSelect";
import SubmitLayout from '@/components/SubmitLayout';
import ErrorAlert from "@/components/ErrorAlert";
import {readTranslationFromResponse} from '@/utils/data-managment';

export default function Form() {
    const { id } = useParams();
    const client = useClient();
    const queryClient = useQueryClient();
    const navigate = useNavigate();
    const countriesRoute = getRouteWithLang("/countries");
    const [backendErrors, setBackendErrors] = React.useState([]);

    const Schema = Yup.object().shape({
        name_ar: Yup.string().required("name_ar_is_required"),
        name_en: Yup.string().required("name_en_is_required"),
        code: Yup.number().required("code_is_required").typeError('code_is_required'),
        symbol: Yup.string().required("symbol_is_required"),
        currency: Yup.object().typeError("currency_is_required").required("currency_is_required"),
    });

    const { isLoading: fetchLoading, data: country } = useQuery({
        queryKey: `country_${id}`,
        queryFn: () => client(`countries/${id}`).then((data) => data.data),
        enabled: id !== undefined,
    });

    const {
        control,
        handleSubmit,
        reset,
        formState: { errors, isDirty },
    } = useForm({
        resolver: yupResolver(Schema),
        defaultValues: {
            name_ar: "",
            name_en: "",
            code:"",
            symbol:"",
            currency:""
        },
    });

    useEffect(() => {
        if (country && id !== undefined) {
            reset({
                ...readTranslationFromResponse(country)
            })
        }
    }, [country]);

    const {mutate, isError, isLoading} = useMutation(
        data =>
          client(`${id ? `countries/${id}` : 'countries'} `, {
            method: id ? 'PUT' : 'POST',
            data,
          }),
        {
          onSuccess: () => {
            queryClient.invalidateQueries('countries')
            navigate(`${countriesRoute}`)
            reset()
            if (id) successWithCustomMessage('updated_success_msg')
            else successWithCustomMessage('added_success_msg')
          },
          onError: error => {
            let errors = getErrorsFromResponse(error)
            setBackendErrors(errors)
          },
        },
    )

    const onSubmitForm = ({name_en, name_ar,code,symbol,currency}) => {
        mutate({
            en: {
                name: name_en,
            },
            ar: {
                name: name_ar,
            },
            code,
            symbol,
            currency_id:currency?.id
        })
    }

    if (fetchLoading) {
        return <FullPageSpinner />;
    }

    return (
        <>
            <form
                autoComplete="off"
                noValidate
                onSubmit={handleSubmit(onSubmitForm)}
            >
                <Stack spacing={3}>
                    <ErrorAlert isError={isError} errors={backendErrors} />
                    <CustomInput
                        label="name_ar"
                        name="name_ar"
                        control={control}
                        errors={errors}
                    />
                    <CustomInput
                        label="name_en"
                        name="name_en"
                        control={control}
                        errors={errors}
                    />
                    <CustomInput
                        label="code"
                        name="code"
                        control={control}
                        errors={errors}
                    />
                    <CustomInput
                        label="symbol"
                        name="symbol"
                        control={control}
                        errors={errors}
                    />
                    <AsyncSelect
                        title="currency"
                        name="currency"
                        optionUrl="currencies"
                        control={control}
                        errors={errors}
                        optionLabel="name"
                    />

                </Stack>
                <SubmitLayout
                    isLoading={isLoading}
                    isDisabled={!isDirty}
                    label={id !== undefined ? 'update' : 'save'}
                    cancelAction={()=> navigate(-1)}
                />
            </form>
        </>
    );
}
