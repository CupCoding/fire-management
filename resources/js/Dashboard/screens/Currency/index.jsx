import * as React from "react";
import { Button, Container, Stack, styled, Typography } from "@mui/material";
import ReactTableV2 from "@/components/ReactTableV2";
import { useMemo } from "react";
import { Link as RouterLink } from "react-router-dom";
import Iconify from "@/components/Iconify";
import Page from "@/components/Page";
import { tableColumns } from "./data";
import { useQuery } from "react-query";
import { useClient } from "@/context/auth-context";
import { FormattedMessage } from "react-intl";
import { getRouteWithLang } from "@/utils/routesHelpers";
import Breadcrumbs from "@/components/Breadcrumbs";
import queryString from "query-string";
import { readQueryParams } from '@/hooks/useFilter';
import {errorWithCustomMessage} from "@/utils/notifications";

export default function Countries() {
    const columns = useMemo(() => tableColumns, []);
    const client = useClient();
    const fetchDataOptions = readQueryParams()

    const { data: currencies, isLoading } = useQuery(
        ["currencies", fetchDataOptions],
        () => client(`currencies?${queryString.stringify(fetchDataOptions)}`),
        {
            keepPreviousData: true,
            onError: () => {
                errorWithCustomMessage("failed_with_reload_msg");
            },
        }
    );

    return (
        <Page title="All Countries">
            <Container>
                <Stack
                    direction="row"
                    alignItems="center"
                    justifyContent="space-between"
                    mb={1}
                >
                    <Typography variant="h4">
                        <FormattedMessage id="currencies" />
                    </Typography>

                    <Button
                        variant="contained"
                        component={RouterLink}
                        to={getRouteWithLang("/currencies/add")}
                        startIcon={<Iconify icon="eva:plus-fill" />}
                    >
                        <FormattedMessage id="new" />
                    </Button>
                </Stack>
                <Breadcrumbs />
                <ReactTableV2
                    columns={columns}
                    tableData={currencies}
                    isToolbar={true}
                    isLoading={isLoading}
                />
            </Container>
        </Page>
    );
}
