import React from "react";
import MoreMenu from "./Partials/MoreMenu";

export const tableColumns = [
    {
        header: "name",
        accessorKey: "name",
    },
    {
        header: "code",
        accessorKey: "code",
        enableSorting: false,
    },
    {
        header: "exchange_rate",
        accessorKey: "exchange_rate",
        enableSorting: false,
    },

    {
        header: "actions",
        accessorKey: "actions",
        cell: ({ row }) => (<MoreMenu id={row.original.id}/>),
        enableSorting: false,
    },
];
