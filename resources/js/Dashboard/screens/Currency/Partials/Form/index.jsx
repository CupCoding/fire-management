import CustomInput from "@/components/Form/components/CustomInput";
import { FullPageSpinner } from "@/components/lib";
import { useAuth, useClient } from "@/context/auth-context";
import { getErrorsFromResponse } from "@/utils/fromHelper";
import { successWithCustomMessage } from "@/utils/notifications";
import { getRouteWithLang } from "@/utils/routesHelpers";
import { yupResolver } from "@hookform/resolvers/yup";
import { Stack } from "@mui/material";
import React, { useEffect } from "react";
import { useForm } from "react-hook-form";
import { useMutation, useQuery, useQueryClient } from "react-query";
import { useNavigate, useParams } from "react-router-dom";
import * as Yup from "yup";
import SubmitLayout from '@/components/SubmitLayout';
import ErrorAlert from "@/components/ErrorAlert";
import { readTranslationFromResponse } from "../../../../utils/data-managment";

export default function Form() {
    const { id } = useParams();
    const client = useClient();
    const queryClient = useQueryClient();
    const navigate = useNavigate();
    const currenciesRoute = getRouteWithLang("/currencies");
    const [backendErrors, setBackendErrors] = React.useState([]);

    const Schema = Yup.object().shape({
        name_ar: Yup.string().required("name_ar_is_required"),
        name_en: Yup.string().required("name_en_is_required"),
        code: Yup.string().required("code_is_required"),
        exchange_rate: Yup.string().required("exchange_rate_is_required"),
    });

    const { isLoading: fetchLoading, data: currency } = useQuery({
        queryKey: `currency_${id}`,
        queryFn: () => client(`currencies/${id}`).then((data) => data.data),
        enabled: id !== undefined,
    });

    const {
        control,
        handleSubmit,
        reset,
        formState: { errors, isDirty },
    } = useForm({
        resolver: yupResolver(Schema),
        defaultValues: {
            name_ar: "",
            name_en: "",
            code:"",
            exchange_rate:""
        },
    });

    useEffect(() => {
        if (currency && id !== undefined) {
            reset({
                ...readTranslationFromResponse(currency)
            })
        }
    }, [currency]);

    const {mutate, isError, isLoading} = useMutation(
        data =>
          client(`${id ? `currencies/${id}` : 'currencies'} `, {
            method: id ? 'PUT' : 'POST',
            data,
          }),
        {
          onSuccess: () => {
            queryClient.invalidateQueries('currencies')
            navigate(`${currenciesRoute}`)
            reset()
            if (id) successWithCustomMessage('updated_success_msg')
            else successWithCustomMessage('added_success_msg')
          },
          onError: error => {
            let errors = getErrorsFromResponse(error)
            setBackendErrors(errors)
          },
        },
    )

    const onSubmitForm = ({name_en, name_ar,code,exchange_rate}) => {
        mutate({
            en: {
                name: name_en,
            },
            ar: {
                name: name_ar,
            },
            code,
            exchange_rate,
        })
    }

    if (fetchLoading) {
        return <FullPageSpinner />;
    }

    return (
        <>
            <form
                autoComplete="off"
                noValidate
                onSubmit={handleSubmit(onSubmitForm)}
            >
                <Stack spacing={3}>
                    <ErrorAlert isError={isError} errors={backendErrors} />

                    <CustomInput
                        label="name_ar"
                        name="name_ar"
                        control={control}
                        errors={errors}
                    />
                    <CustomInput
                        label="name_en"
                        name="name_en"
                        control={control}
                        errors={errors}
                    />
                    <CustomInput
                        label="code"
                        name="code"
                        control={control}
                        errors={errors}
                    />
                    <CustomInput
                        label="exchange_rate"
                        name="exchange_rate"
                        control={control}
                        errors={errors}
                    />


                </Stack>
                <SubmitLayout
                    isLoading={isLoading}
                    isDisabled={!isDirty}
                    label={id !== undefined ? 'update' : 'save'}
                    cancelAction={()=> navigate(-1)}
                />
            </form>
        </>
    );
}
