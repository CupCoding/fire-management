import {Container, Grid, Stack, Typography} from '@mui/material'
import Page from '@/components/Page'
import React from 'react'
import {FormattedMessage} from 'react-intl'
import Breadcrumbs from '@/components/Breadcrumbs'
import { useParams } from 'react-router'
import {useClient} from '@/context/auth-context';
import {useQuery} from 'react-query';
import {FullPageSpinner} from '@/components/lib';

export default function Edit() {
  const {id} = useParams()
  const client = useClient()

  const {isLoading , data} = useQuery({
    queryKey: `review-${id}`,
    queryFn: () => client(`reviews/${id}`).then(data => data.data),
    enabled: id !== undefined,
  })

  let fileds = ['review','rate']

  if (isLoading) {
    return <FullPageSpinner />
  }

  return (
    <Page title="Review">
      <Container>
        <Stack
          direction="row"
          alignItems="center"
          justifyContent="space-between"
        >
          <Typography variant="h4" gutterBottom>
            <FormattedMessage id="review" /> {id}
          </Typography>
        </Stack>
        <Breadcrumbs />
        <Grid container>
            {fileds.map(filed => {
                return <>
                    <Grid item xs={6} sx={{ borderBottom:'1px solid' }}>
                        <FormattedMessage id={filed} />
                    </Grid>
                    <Grid item xs={6} sx={{ borderBottom:'1px solid' }}>
                        {data[filed]}
                    </Grid >
                </>
            })}
        </Grid>
      </Container>
    </Page>
  )
}
