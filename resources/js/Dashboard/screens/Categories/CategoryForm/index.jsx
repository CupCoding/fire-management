import CustomInput from '@/components/Form/components/CustomInput'
import {FullPageSpinner} from '@/components/lib'
import {useAuth, useClient} from '@/context/auth-context'
import {getErrorsFromResponse} from '@/utils/fromHelper'
import {successWithCustomMessage} from '@/utils/notifications'
import {getRouteWithLang} from '@/utils/routesHelpers'
import {yupResolver} from '@hookform/resolvers/yup'
import {LoadingButton} from '@mui/lab'
import {Alert, Stack} from '@mui/material'
import axios from 'axios'
import * as React from 'react'
import {useEffect} from 'react'
import {useForm} from 'react-hook-form'
import {FormattedMessage} from 'react-intl'
import {useMutation, useQuery, useQueryClient} from 'react-query'
import {useLocation, useNavigate, useParams} from 'react-router-dom'
import * as Yup from 'yup'
import CustomTextarea from '@/components/Form/components/CustomTextarea';
// ----------------------------------------------------------------------

export default function CategoryForm({onSubmit}) {
  const {id} = useParams()
  const {state} = useLocation()
  const client = useClient()
  const queryClient = useQueryClient()
  const navigate = useNavigate()
  const user = useAuth()
  const categoriesRoute = getRouteWithLang('/categories')
  const CategorySchema = Yup.object().shape({
    name_ar: Yup.string().required('name_ar_is_required'),
    name_en: Yup.string().required('name_en_is_required'),
    description_en: Yup.string().required('description_en_is_required'),
    description_ar: Yup.string().required('description_ar_is_required'),

  })
  const [backendErrors, setBackendErrors] = React.useState([])
  const {
    control,
    handleSubmit,
    reset,
    formState: {errors, isDirty},
  } = useForm({
    resolver: yupResolver(CategorySchema),
    defaultValues: {
      name_en: '',
      name_ar: '',
      description_ar:'',
      description_en:'',
      image: '',
    },
  })

  const {isLoading: fetchLoading, data: category} = useQuery({
    queryKey: 'category',
    queryFn: () => client(`categories/${id}`).then(data => data.data),
    enabled: id !== undefined,
  })

  useEffect(() => {
    if (category && id !== undefined) {
        let receivedCategory = {}
        category?.translations.map(lang =>{
            if(lang.locale === 'ar'){
                receivedCategory.name_ar = lang.name
                receivedCategory.description_ar = lang.description
            }
            else{
                receivedCategory.name_en = lang.name
                receivedCategory.description_en = lang.description
            }
        })
      reset({
        ...receivedCategory
     })
    }
  }, [category])

  const {mutate, isError, isLoading} = useMutation(
    data =>
      client(`${id ? `categories/${id}` : 'categories'} `, {
        method: id ? 'PUT' : 'POST',
        data,
      }),
    {
      onSuccess: () => {
        queryClient.invalidateQueries('categories')
        navigate(`${categoriesRoute}`)
        reset()
        if (id) successWithCustomMessage('updated_success_msg')
        else successWithCustomMessage('added_success_msg')
      },
      onError: error => {
        let errors = getErrorsFromResponse(error)
        setBackendErrors(errors)
      },
    },
  )
  const onSubmitForm = ({name_en, name_ar,description_en,description_ar}) => {
    let parent = null;
    if (state === null && !id) {
        //add parent
            parent = null
      } else if (state !== null && !id) {
        //add child
            parent = state?.id
      } else {
        //edit
        if (state.parent === null) {
          //edit parent
            parent = null
        } else {
          //edit child
            parent = category?.parent?.id
        }
      }
    mutate({
        parent_id: parent ?? undefined,
        en: {
            name: name_en,
            description: description_en
        },
        ar: {
            "name": name_ar,
            "description": description_ar
        }
    })
  }

  if (fetchLoading) {
    return <FullPageSpinner />
  }
  return (
    <form noValidate onSubmit={handleSubmit(onSubmitForm)}>
      <Stack spacing={3}>
        {backendErrors.length !== 0 ? (
          <Alert severity="error">
            {backendErrors.map(({filedName, errorMessage}) => {
              return (
                <div>
                  <FormattedMessage id={filedName} />
                  <p>{errorMessage}</p>
                </div>
              )
            })}
          </Alert>
        ) : null}
        <CustomInput
          autoFocus
          label="category_name_en"
          name="name_en"
          control={control}
          errors={errors}
        />
        <CustomInput
          label="category_name_ar"
          name="name_ar"
          control={control}
          errors={errors}
        />

        <CustomTextarea
            label="category_name_ar"
            name="description_ar"
            control={control}
            errors={errors}
        />

        <CustomTextarea
            label="category_name_ar"
            name="description_en"
            control={control}
            errors={errors}
        />

      </Stack>
      <Stack
        direction="row"
        alignItems="center"
        justifyContent="flex-end"
        sx={{my: 2}}
      >
        <LoadingButton
          onClick={() => navigate(`${categoriesRoute}`)}
          size="large"
          variant="contained"
          sx={{mr: 2}}
        >
          <FormattedMessage id="cancel" />
        </LoadingButton>
        <LoadingButton
          size="large"
          type="submit"
          variant="contained"
          loading={isLoading && !isError}
          disabled={!isDirty}
        >
          {id ? (
            <FormattedMessage id="update_category" />
          ) : (
            <FormattedMessage id="save" />
          )}
        </LoadingButton>
      </Stack>
    </form>
  )
}
