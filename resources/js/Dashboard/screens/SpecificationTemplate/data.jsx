import React from "react";
import MoreMenu from "./Partials/MoreMenu";

export const tableColumns = [
    {
        header: "title",
        accessorKey: "title",
    },
    {
        header: "type",
        accessorKey: "type_name",
    },
    {
        header: "actions",
        accessorKey: "actions",
        cell: ({ row }) => (<MoreMenu id={row.original.id}/>),
        enableSorting: false,
    },
];
