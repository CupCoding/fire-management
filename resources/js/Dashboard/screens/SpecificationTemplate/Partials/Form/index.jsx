import CustomInput from "@/components/Form/components/CustomInput";
import { FullPageSpinner } from "@/components/lib";
import { useAuth, useClient } from "@/context/auth-context";
import { getErrorsFromResponse } from "@/utils/fromHelper";
import { successWithCustomMessage } from "@/utils/notifications";
import { getRouteWithLang } from "@/utils/routesHelpers";
import { yupResolver } from "@hookform/resolvers/yup";
import { Stack } from "@mui/material";
import React, { useEffect } from "react";
import { useForm } from "react-hook-form";
import { useMutation, useQuery, useQueryClient } from "react-query";
import { useNavigate, useParams } from "react-router-dom";
import * as Yup from "yup";
import AsyncSelect from '@/components/Form/components/AsyncSelect/index';
import ErrorAlert from "@/components/ErrorAlert";
import Breadcrumbs from '@mui/material/Breadcrumbs';
import NavigateNextIcon from '@mui/icons-material/NavigateNext';
import Chip from '@mui/material/Chip';
import SubmitLayout from '@/components/SubmitLayout';
import { readTranslationFromResponse } from "../../../../utils/data-managment";

export default function Form() {
    const { id } = useParams();
    const client = useClient();
    const queryClient = useQueryClient();
    const navigate = useNavigate();
    const specificationsRoute = getRouteWithLang("/templates");
    const [backendErrors, setBackendErrors] = React.useState([]);

    const Schema = Yup.object().shape({
        title_ar: Yup.string().required("title_ar_is_required"),
        title_en: Yup.string().required("title_en_is_required"),
        type : Yup.string().required('type_is_required'),
        data: Yup.array().min(1,'data_is_required').typeError('data_is_required').required('data_is_required')
    });

    const { isLoading: fetchLoading, data: template } = useQuery({
        queryKey: `template_${id}`,
        queryFn: () => client(`specification-templates/${id}`).then((data) => data.data),
        enabled: id !== undefined,
    });

    const {
        control,
        handleSubmit,
        reset,
        watch,
        formState: { errors, isDirty },
    } = useForm({
        resolver: yupResolver(Schema),
        defaultValues: {
            title_ar: "",
            title_en: "",
            type:"",
            data:[]
        },
    });

    useEffect(() => {
        if (template && id !== undefined) {
            reset({
                ...readTranslationFromResponse(template)
            })
        }
    }, [template]);

    const {mutate, isError, isLoading} = useMutation(
        data =>
          client(`${id ? `specification-templates/${id}` : 'specification-templates'} `, {
            method: id ? 'PUT' : 'POST',
            data,
          }),
        {
          onSuccess: () => {
            queryClient.invalidateQueries('specification-templates')
            navigate(`${specificationsRoute}`)
            reset()
            if (id) successWithCustomMessage('updated_success_msg')
            else successWithCustomMessage('added_success_msg')
          },
          onError: error => {
            let errors = getErrorsFromResponse(error)
            setBackendErrors(errors)
          },
        },
      )
      const onSubmitForm = ({title_en, title_ar, type, data}) => {
        mutate({
            en: {
                title: title_en,
            },
            ar: {
                title: title_ar,
            },
            type,
            data:data.map(item => item.id)
        })
      }

    if (fetchLoading) {
        return <FullPageSpinner />
    }

    return (
        <>
            <form
                autoComplete="off"
                noValidate
                onSubmit={handleSubmit(onSubmitForm)}
            >
                <Stack spacing={3}>
                    <ErrorAlert isError={isError} errors={backendErrors} />

                    <CustomInput
                        label="title_ar"
                        name="title_ar"
                        control={control}
                        errors={errors}
                    />

                    <CustomInput
                        label="title_en"
                        name="title_en"
                        control={control}
                        errors={errors}
                    />

                    <CustomInput
                        label="type"
                        name="type"
                        control={control}
                        errors={errors}
                    />

                    <AsyncSelect
                        title="specification"
                        name="data"
                        optionUrl="specifications"
                        control={control}
                        errors={errors}
                        optionLabel="name"
                        multiple={true}
                    />
                      <Breadcrumbs
                        separator={<NavigateNextIcon fontSize="small" />}
                        aria-label="breadcrumb"
                        sx={{
                            display:'flex',
                            justifyContent:'center'
                    }}
                    >
                        {watch('data')?.map((item,index) => <Chip label={`${index+1}-${item.name}`} variant="outlined"/>)}
                    </Breadcrumbs>
                </Stack>
                <SubmitLayout
                    isLoading={isLoading}
                    isDisabled={!isDirty}
                    label={id !== undefined ? 'update' : 'save'}
                    cancelAction={()=> navigate(-1)}
                />
            </form>
        </>
    );
}
