import CustomInput from "@/components/Form/components/CustomInput";
import DropzoneField from "@/components/Form/components/DropzoneField";
import InputPassword from "@/components/Form/components/InputPassword";
import PhoneNumber from "@/components/Form/components/PhoneNumber";
import { FullPageSpinner } from "@/components/lib";
import { useClient } from "@/context/auth-context";
import { getErrorsFromResponse } from "@/utils/fromHelper";
import { successWithCustomMessage } from "@/utils/notifications";
import { getRouteWithLang } from "@/utils/routesHelpers";
import { yupResolver } from "@hookform/resolvers/yup";
import { Stack } from "@mui/material";
import React, { useEffect } from "react";
import { useForm } from "react-hook-form";
import { useMutation, useQuery, useQueryClient } from "react-query";
import { useNavigate, useParams } from "react-router-dom";
import * as Yup from "yup";
import CustomCheckbox from "@/components/Form/components/CustomCheckbox";
import SubmitLayout from '@/components/SubmitLayout';
import ErrorAlert from "@/components/ErrorAlert";
import {prepareDataForm} from '@/utils/data-managment';

export default function UserForm() {
    const { id } = useParams();
    const client = useClient('multipart/form-data');
    const queryClient = useQueryClient();
    const navigate = useNavigate();
    const usersRoute = getRouteWithLang("/users");
    const [backendErrors, setBackendErrors] = React.useState([]);

    const UserSchema = Yup.object().shape({
        name: Yup.string().required("name_is_required"),
        email: Yup.string()
            .email("valid_email_address")
            .required("email_is_required"),
        password: id
            ? ""
            : Yup.string()
                  .typeError("password_is_required")
                  .required()
                  .matches(
                      /^(?=.*[A-Z])(?=.*[a-z])?(?=.*\d)(?=.*[~@$!%*#?&()\-_=+\[\]{};:'"\\/<>|`,.])?[A-Za-z\d~@$!%*#?&()\-_=+\[\]{};:'"\\/<>|`,.]{8,}$/,
                      "password_must_contain_8_Characters_one_capital_character_and_one_number_at_least"
                  ),
        phoneNumber: Yup.string()
            .min(10, "phone_number_must_be_at_least_10_characters")
            .max(12, "phone_number_must_be_at_most_12_characters")
            .required("phone_number_is_required"),
        avatar: Yup.mixed().nullable(),
        isActive: Yup.boolean().nullable(),
    });

    const { isLoading: fetchLoading, data: user } = useQuery({
        queryKey: `user_${id}`,
        queryFn: () => client(`users/${id}`).then((data) => data.data),
        enabled: id !== undefined,
    });

    const {
        control,
        handleSubmit,
        reset,
        trigger,
        watch,
        setValue,
        formState: { errors, isDirty },
    } = useForm({
        resolver: yupResolver(UserSchema),
        defaultValues: {
            isActive: null,
            name: "",
            phoneNumber: "",
            password: "",
            email: "",
            avatar: "",
        },
    });

    const onFileChange = async (name, files) => {
        setValue(name, files, { shouldDirty: true });
        if (files?.file) {
            await trigger(["avatar"]);
        }
    };

    useEffect(() => {
        if (user && id !== undefined) {
            reset({
                ...user,
                isActive: user?.data?.is_active,
                name: user?.data?.name,
                email: user?.data?.email,
                phoneNumber: user?.data?.phone_number,
                password: user?.data?.password,
                avatar: user?.data?.avatarFileUrl,
            });
        }
    }, [user]);

    const { mutate, isError, isLoading } = useMutation(
        (data) =>
            client(`${id ? `users/${id}` : 'users'} `, {
            method: 'POST',
            data,
            }),
        {
            onSuccess: () => {
                queryClient.invalidateQueries("users");
                navigate(`${usersRoute}`);
                reset();
                if (id) successWithCustomMessage("updated_success_msg");
                else successWithCustomMessage("added_success_msg");
            },
            onError: (error) => {
                let errors = getErrorsFromResponse(error);
                setBackendErrors(errors);
            },
        }
    );

    const onSubmitForm = (
    data) => {
        let formData = prepareDataForm(data)
        !data.avatar
            ? formData.append("avatar", null)
            : formData.append("avatar", data.avatar);
        data.password != null && formData.append("password", data.password);
        data.password != null && formData.append("password_confirmation", data.password);
        id && formData.append("_method", "PUT");
        mutate(formData);
    };

    if (fetchLoading) {
        return <FullPageSpinner />;
    }

    return (
        <>
            <form
                autoComplete="off"
                noValidate
                onSubmit={handleSubmit(onSubmitForm)}
            >
                <Stack spacing={3}>
                    <ErrorAlert isError={isError} errors={backendErrors} />

                    <CustomCheckbox
                        label="isActive"
                        name="isActive"
                        control={control}
                    />

                    <CustomInput
                        label="email"
                        name="email"
                        control={control}
                        errors={errors}
                    />
                    <CustomInput
                        label="name"
                        name="name"
                        control={control}
                        errors={errors}
                    />

                    <PhoneNumber
                        label="phone_number"
                        name="phoneNumber"
                        control={control}
                        errors={errors}
                        dialcode={watch("country")?.dialCode}
                    />

                    <InputPassword
                        label="password"
                        name="password"
                        control={control}
                        errors={errors}
                    />

                    <DropzoneField
                        name="avatar"
                        control={control}
                        InputChange={(name, files) => onFileChange(name, files)}
                        errors={errors}
                        editValue={user?.data?.avatarFileUrl}
                    />
                </Stack>
                <SubmitLayout
                    isLoading={isLoading}
                    isDisabled={!isDirty}
                    label={id !== undefined ? 'update' : 'save'}
                    cancelAction={()=> navigate(-1)}
                />
            </form>
        </>
    );
}
