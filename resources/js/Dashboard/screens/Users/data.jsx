import { Avatar, Chip, Stack, Typography } from "@mui/material";
import React from "react";
import MoreMenu from "./Partials/MoreMenu";
import { getDateFromISO } from "@/utils/formatTime";
import { useAuth } from "@/context/auth-context";
import Label from "@/components/Label";
let storage = import.meta.env.VITE_STORAGE;

export const tableColumns = [
    {
        header: "name",
        accessorKey: "name",
        style: {
            padding: "0",
        },
        cell: ({ row }) => (
            <Stack direction="row" alignItems="center" spacing={2}>
                <Avatar
                    alt={row.original.name}
                    src={
                        row.original.avatar &&
                        `${storage}/${row.original.avatar}`
                    }
                />
                <Typography variant="subtitle2" noWrap>
                    {row.original.name}
                </Typography>
            </Stack>
        ),
    },
    {
        header: "email",
        accessorKey: "email",
        enableSorting: false,
    },
    {
        header: "active",
        accessorKey: "is_active",
        cell: ({ row }) => {
            let label = row?.original?.is_active ? "true" : "false";
            return (
                <Label
                    variant="ghost"
                    color={label === "true" ? "success" : "error"}
                    title={label}
                />
            );
        },
    },
    {
        header: "created_at",
        cell: ({ row }) => (
            <span>{getDateFromISO(row?.original?.created_at)}</span>
        ),
    },
    {
        header: "actions",
        accessorKey: "actions",
        cell: ({ row }) => {
            const { user: userAuth } = useAuth();
            if (row.original.id !== userAuth.id)
                return (
                    <MoreMenu
                        id={row.original.id}
                        fullName={row?.original?.fullName}
                    />
                );
            return <></>;
        },
        style: {
            textAlign: "right",
        },
        enableSorting: false,
    },
];
