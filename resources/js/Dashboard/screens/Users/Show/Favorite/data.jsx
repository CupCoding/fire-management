import { Avatar, Stack, Typography } from "@mui/material";
import React from "react";
import Label from "@/components/Label";
let storage = import.meta.env.VITE_STORAGE;

export const tableColumns = [
    {
        header: "product",
        cell: ({ row }) => (
            <Stack direction="row" alignItems="center" spacing={2}>
                <Avatar
                    variant="square"
                    alt={row.original.name}
                    src={
                        row.original.avatar &&
                        `${storage}/${row.original.avatar}`
                    }
                />
                <Typography variant="subtitle2" noWrap>
                    {row.original.product.name}
                </Typography>
            </Stack>
        ),
        enableSorting: false,
    },
    {
        header: "description",
        cell: ({ row }) => (
            <Typography variant="subtitle2" noWrap>
                {row.original.product.description}
            </Typography>
        ),
        enableSorting: false,
    },
    {
        header: "category",
        cell: ({ row }) => (
            <Label title={row?.original?.product?.category?.name}/>
        ),
    },
];
