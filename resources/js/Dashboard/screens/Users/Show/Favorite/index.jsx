import React from 'react';
import {Button, Stack, Typography, Container} from '@mui/material';
import {Link as RouterLink} from 'react-router-dom';
import Iconify from '@/components/Iconify';
import {FormattedMessage} from 'react-intl';
import {getRouteWithLang} from '@/utils/routesHelpers';
import Page from '@/components/Page';
import ReactTableV2 from '@/components/ReactTableV2';
import {useQuery} from 'react-query';
import {useParams} from 'react-router-dom';
import {useMemo} from 'react';
import {tableColumns} from './data';
import {useClient} from '@/context/auth-context';
import {errorWithCustomMessage} from "@/utils/notifications";

const Favorites = () => {
    const {id} = useParams()
    const columns = useMemo(() => tableColumns, []);
    const client = useClient();
    const { data: favorites, isLoading } = useQuery(
        ["favorites"],
        () => client(`favorites?$user_id=${id}`),
        {
            keepPreviousData: true,
            onError: () => {
                errorWithCustomMessage("failed_with_reload_msg");
            },
        }
    );
  return (
        <Page title="User favorites">
            <Container>
                <Stack
                    direction="row"
                    alignItems="center"
                    justifyContent="space-between"
                    mb={2}
                >
                    <Typography variant="h4">
                        <FormattedMessage id="user_favorites" />
                    </Typography>
                </Stack>
                <ReactTableV2
                    columns={columns}
                    tableData={favorites}
                    isToolbar={false}
                    isLoading={isLoading}
                />
            </Container>
        </Page>
  );
}

export default Favorites;
