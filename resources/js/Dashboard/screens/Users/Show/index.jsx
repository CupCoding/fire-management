import * as React from 'react';
import { Tabs, Tab, Typography, Box, Container } from '@mui/material';
import { FormattedMessage } from 'react-intl';
import Iconify from '../../../components/Iconify';
import Profile from './Profile';
import Address from './Address';
import Breadcrumbs from '@/components/Breadcrumbs/index';
import Page from '@/components/Page';
import Favourites from './Favorite'

const Show = () => {
const [tab, setTab] = React.useState(0);
    const handleChange = (event, selectedTab) => {
        setTab(selectedTab);
    };

    function a11yProps(index) {
        return {
            id: `simple-tab-${index}`,
            'aria-controls': `simple-tabpanel-${index}`,
        };
    }

    function TabPanel(props) {
        const { children, value, index, ...other } = props;
        return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}

            >
            {value === index && (
                <Box sx={{ p: 3 }}>
                    <Typography component={'span'} >{children}</Typography>
                </Box>
            )}
        </div>
        );
    }

  return (
    <Page title="Show User">

        <Container>
        <Breadcrumbs/>
        <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
            <Tabs value={tab} onChange={handleChange} aria-label="basic tabs example">
                <Tab label={
                    <Box>
                        <Iconify icon="carbon:user-profile" sx={{mr:1}} />
                        <FormattedMessage id="profile"/>
                    </Box>
                    } {...a11yProps(0)}
                />
                <Tab label={
                    <Box>
                        <Iconify icon="mdi:address-marker-outline" sx={{mr:1}} />
                        <FormattedMessage id="address"/>
                    </Box>} {...a11yProps(1)}
                />
                <Tab label={
                    <Box>
                        <Iconify icon="mdi:cards-heart-outline" sx={{mr:1}} />
                        <FormattedMessage id="favorites"/>
                    </Box>} {...a11yProps(1)}
                />
            </Tabs>
        </Box>
        <TabPanel component='div' value={tab} index={0}>
            <Profile />
        </TabPanel>
        <TabPanel component='div' value={tab} index={1}>
            <Address />
        </TabPanel>
        <TabPanel component='div' value={tab} index={2}>
            <Favourites />
        </TabPanel>
        </Container>
    </Page>
  );
}

export default Show;


