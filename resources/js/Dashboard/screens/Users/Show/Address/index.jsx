import React from 'react';
import Map from './Map';
import {Button, Stack, Typography, Container} from '@mui/material';
import {Link as RouterLink} from 'react-router-dom';
import Iconify from '@/components/Iconify';
import {FormattedMessage} from 'react-intl';
import {getRouteWithLang} from '@/utils/routesHelpers';
import Page from '@/components/Page';
import ReactTableV2 from '@/components/ReactTableV2';
import {useQuery} from 'react-query';
import {useParams} from 'react-router-dom';
import {useMemo} from 'react';
import {tableColumns} from './data';
import {useClient} from '@/context/auth-context';
import {
    errorWithCustomMessage,
} from "@/utils/notifications";

const Address = () => {
    const {id} = useParams()
    const columns = useMemo(() => tableColumns, []);
    const client = useClient();
    const { data: addresses, isLoading } = useQuery(
        ["addresses"],
        () => client(`addresses?$user_id=${id}`),
        {
            keepPreviousData: true,
            onError: () => {
                errorWithCustomMessage("failed_with_reload_msg");
            },
        }
    );
  return (
    <>
        <Page title="User Addresses">
            <Container>
                <Stack
                    direction="row"
                    alignItems="center"
                    justifyContent="space-between"
                    mb={2}
                >
                    <Typography variant="h4">
                        <FormattedMessage id="address" />
                    </Typography>

                    <Button
                        variant="contained"
                        component={RouterLink}
                        to={getRouteWithLang(`/users/${id}/address/add`)}
                        startIcon={<Iconify icon="eva:plus-fill" />}
                    >
                        <FormattedMessage id="new" />
                    </Button>
                </Stack>
                {addresses?.data.length > 0 && <Map
                    initalAddress={[addresses.data[0].latitude,addresses.data[0].longitude]}
                    marks={addresses?.data?.map(address => [address?.latitude,address?.longitude])}
                />}
                <ReactTableV2
                    columns={columns}
                    tableData={addresses}
                    isToolbar={false}
                    isLoading={isLoading}
                />
            </Container>
        </Page>

    </>
  );
}

export default Address;
