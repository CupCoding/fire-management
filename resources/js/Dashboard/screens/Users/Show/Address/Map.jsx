import React, { useEffect, useRef } from "react";
import L from "leaflet";
import "leaflet/dist/leaflet.css";
import { Box } from "@mui/system";

function Map({ onClick, marks, initalAddress }) {
    const mapRef = useRef(null);
    let prevMark = [-99, -99];

    const getPosition = () =>
        new Promise((res, rej) => {
            navigator.geolocation.getCurrentPosition(res, rej);
        });

    useEffect(() => {
        (async () => {
            if (!initalAddress) {
                let position = await getPosition().catch(() => {});
                initalAddress = position
                    ? [position?.coords?.latitude, position?.coords?.longitude]
                    : initalAddress;
            }
            if (mapRef.current) {
                const map = L.map(mapRef.current).setView(initalAddress, 13);
                L.tileLayer(
                    "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                ).addTo(map);
                marks?.map((mark) => L.marker(mark).addTo(map));
                function onMapClick(e) {
                    let mark;
                    mark = onClick(e);
                    mark = L.marker(mark);
                    map.addLayer(mark);
                    if (prevMark[0] === -99 && prevMark[1] === -99) {
                        prevMark = mark;
                    } else {
                        map.removeLayer(prevMark);
                        prevMark = mark;
                    }
                }
                map.on("click", onMapClick);
            }
        })();
    }, []);

    return (
        <Box sx={{ my: 2 }}>
            <div ref={mapRef} style={{ width: "100%", height: "400px" }}></div>
        </Box>
    );
}

export default Map;
