import CustomInput from "@/components/Form/components/CustomInput";
import { FullPageSpinner } from "@/components/lib";
import { useClient } from "@/context/auth-context";
import { getErrorsFromResponse } from "@/utils/fromHelper";
import { successWithCustomMessage } from "@/utils/notifications";
import { getRouteWithLang } from "@/utils/routesHelpers";
import { yupResolver } from "@hookform/resolvers/yup";
import { LoadingButton } from "@mui/lab";
import { Alert, Stack } from "@mui/material";
import React, { useEffect } from "react";
import { useForm } from "react-hook-form";
import { FormattedMessage } from "react-intl";
import { useMutation, useQuery, useQueryClient } from "react-query";
import { useNavigate, useParams } from "react-router-dom";
import * as Yup from "yup";
import AsyncSelect from '@/components/Form/components/AsyncSelect/index';
import Map from "../../Map";
import { readTranslationFromResponse } from "../../../../../../utils/data-managment";

export default function Form() {
    const { addressId } = useParams();
    const client = useClient();
    const queryClient = useQueryClient();
    const navigate = useNavigate();
    const addressesRoute = getRouteWithLang("/users");
    const [backendErrors, setBackendErrors] = React.useState([]);
    const {id} = useParams()

    const Schema = Yup.object().shape({
        details_en: Yup.string().required("details_en_is_required"),
        details_ar: Yup.string().required("details_ar_is_required"),
        country: Yup.object().required("country_is_required").typeError("country_is_required"),
        longitude: Yup.string().required("longitude_is_required"),
        latitude: Yup.string().required("latitude_is_required"),
    });

    const { isLoading: fetchLoading, data: address } = useQuery({
        queryKey: `address_${addressId}`,
        queryFn: () => client(`addresses/${addressId}`).then((data) => data.data),
        enabled: addressId !== undefined,
    });

    const {
        control,
        handleSubmit,
        reset,
        setValue,
        getValues,
        formState: { errors, isDirty },
    } = useForm({
        resolver: yupResolver(Schema),
        defaultValues: {
            details_en: "",
            details_ar: "",
            country:"",
            longitude:"",
            latitude:""
        },
    });

    useEffect(() => {
        if (address && addressId !== undefined) {
            reset({
                ...readTranslationFromResponse(address)
            })
        }
    }, [address]);

    const {mutate, isError, isLoading} = useMutation(
        data =>
          client(`${addressId ? `addresses/${addressId}` : 'addresses'} `, {
            method: addressId ? 'PUT' : 'POST',
            data,
          }),
        {
          onSuccess: () => {
            queryClient.invalidateQueries('addresses')
            navigate(`${addressesRoute}`)
            reset()
            if (addressId) successWithCustomMessage('updated_success_msg')
            else successWithCustomMessage('added_success_msg')
          },
          onError: error => {
            let errors = getErrorsFromResponse(error)
            setBackendErrors(errors)
          },
        },
    )


    const onSubmitForm = ({country, details_en,details_ar,longitude,latitude}) => {
        mutate({
            en: {
                details: details_en,
            },
            ar: {
                details: details_ar,
            },
            country_id:country.id,
            longitude,
            latitude,
            user_id:id
        })
    }

    if (fetchLoading) {
        return <FullPageSpinner />;
    }

    return (
        <>
            <form
                autoComplete="off"
                noValidate
                onSubmit={handleSubmit(onSubmitForm)}
            >
                <Stack spacing={3}>
                    {isError ? (
                        <Alert severity="error">
                            {backendErrors.map(
                                ({ filedName, errorMessage }) => {
                                    return (
                                        <div>
                                            <FormattedMessage id={filedName} />
                                            <p>{errorMessage}</p>
                                        </div>
                                    );
                                }
                            )}
                        </Alert>
                    ) : null}

                    <CustomInput
                        label="details_ar"
                        name="details_ar"
                        control={control}
                        errors={errors}
                    />
                    <CustomInput
                        label="details_en"
                        name="details_en"
                        control={control}
                        errors={errors}
                    />
                    <AsyncSelect
                        title="country"
                        name="country"
                        optionUrl="countries"
                        control={control}
                        errors={errors}
                        optionLabel="name"
                    />

                    {
                    addressId ?
                    <Map
                        initalAddress={[address?.data?.latitude,address?.data?.longitude]}
                        marks={[[address?.data?.latitude,address?.data?.longitude]]}
                        onClick={(e)=>{
                            setValue('longitude', e.latlng.lng)
                            setValue('latitude', e.latlng.lat)
                            return [e.latlng.lat,e.latlng.lng]
                        }}
                    />
                    :<Map
                        onClick={(e)=>{
                            setValue('longitude', e.latlng.lng)
                            setValue('latitude', e.latlng.lat)
                            return [e.latlng.lat,e.latlng.lng]
                        }}
                    />
                    }

                </Stack>
                <Stack
                    direction="row"
                    alignItems="center"
                    justifyContent="flex-end"
                    sx={{ my: 2 }}
                >
                    <LoadingButton
                        onClick={() => navigate(-1)}
                        size="large"
                        variant="contained"
                        sx={{ mr: 2 }}
                    >
                        <FormattedMessage id="cancel" />
                    </LoadingButton>
                    <LoadingButton
                        size="large"
                        type="submit"
                        variant="contained"
                        loading={isLoading}
                        disabled={!isDirty}
                    >
                        {addressId !== undefined ? (
                            <FormattedMessage id="update" />
                        ) : (
                            <FormattedMessage id="save" />
                        )}
                    </LoadingButton>
                </Stack>
            </form>
        </>
    );
}
