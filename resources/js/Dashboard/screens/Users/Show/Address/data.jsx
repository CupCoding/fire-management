import React from "react";
import MoreMenu from "./Partials/MoreMenu";
import { getDateFromISO } from "@/utils/formatTime";

export const tableColumns = [
    {
        header: "details",
        accessorKey: "details",
        enableSorting: false,
    },
    {
        header: "country",
        accessorKey: "country.name",
        enableSorting: false,
    },
    {
        header: "created_at",
        cell: ({ row }) => (
            <span>{getDateFromISO(row?.original?.created_at)}</span>
        ),
    },
    {
        header: "actions",
        accessorKey: "actions",
        cell: ({ row }) => <MoreMenu id={row.original.id}/>,
        enableSorting: false,
    },
];
