import * as React from "react";
import { Button, Container, Stack, styled, Typography } from "@mui/material";
import ReactTableV2 from "@/components/ReactTableV2";
import { useMemo } from "react";
import { Link as RouterLink } from "react-router-dom";
import Iconify from "@/components/Iconify";
import Page from "@/components/Page";
import { tableColumns } from "./data";
import { useQuery } from "react-query";
import { useClient } from "@/context/auth-context";
import { FormattedMessage } from "react-intl";
import { getRouteWithLang } from "@/utils/routesHelpers";
import Breadcrumbs from "@/components/Breadcrumbs";
import queryString from "query-string";
import { readQueryParams } from '@/hooks/useFilter';
import {FullPageSpinner} from '@/components/lib';

export default function Products() {
    const columns = useMemo(() => tableColumns, []);
    const client = useClient();
    const fetchDataOptions = readQueryParams();

    const { data: products, isLoading } = useQuery(
        ["products", fetchDataOptions],
        () => client(`products?${queryString.stringify(fetchDataOptions)}`),
        {
            keepPreviousData: true,
            onError: () => {
                errorWithCustomMessage("failed_with_reload_msg");
            },
        }
    );

    if(isLoading){
        return <FullPageSpinner/>
    }

    return (
        <Page title="All Products">
            <Container>
                <Stack
                    direction="row"
                    alignItems="center"
                    justifyContent="space-between"
                    mb={1}
                >
                    <Typography variant="h4">
                        <FormattedMessage id="products" />
                    </Typography>

                    <Button
                        variant="contained"
                        component={RouterLink}
                        to={getRouteWithLang("/products/add")}
                        startIcon={<Iconify icon="eva:plus-fill" />}
                    >
                        <FormattedMessage id="new" />
                    </Button>
                </Stack>
                <Breadcrumbs />
                {/* <FiltersForm /> */}
                <ReactTableV2
                    columns={columns}
                    tableData={products}
                    isToolbar={false}
                    isLoading={isLoading}
                />
            </Container>
        </Page>
    );
}
