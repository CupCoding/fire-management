import { Container, Stack, Typography } from "@mui/material";
import Page from "@/components/Page";
import React from "react";
import { FormattedMessage } from "react-intl";
import Form from "../Partials/Form";
import Breadcrumbs from "@/components/Breadcrumbs";

export default function Add() {
    return (
        <Page title="Add Product">
            <Container>
                <Stack
                    direction="row"
                    alignItems="center"
                    justifyContent="space-between"
                >
                    <Typography variant="h4" gutterBottom>
                        <FormattedMessage id="create_new_product" />
                    </Typography>
                </Stack>
                <Breadcrumbs />
                <Form />
            </Container>
        </Page>
    );
}
