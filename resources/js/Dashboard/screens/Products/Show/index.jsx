import Page from '@/components/Page'
import {Container, Stack, Typography, Tab, Tabs} from '@mui/material'
import React from 'react'
import {FormattedMessage} from 'react-intl'
import Breadcrumbs from '@/components/Breadcrumbs'
import ProductSpecification from './Specification'

function TabPanel({children, value, index, ...other}) {
  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`vertical-tabpanel-${index}`}
      aria-labelledby={`vertical-tab-${index}`}
      {...other}
    >
      {value === index && <>{children}</>}
    </div>
  )
}

function a11yProps(index) {
  return {
    id: `vertical-tab-${index}`,
    'aria-controls': `vertical-tabpanel-${index}`,
  }
}

export default function PriceDetails() {

  const [value, setValue] = React.useState(0)

  const handleChange = (event, newValue) => {
    setValue(newValue)
  }
  return (
    <Page title="Price Details">
      <Container>
        <Stack direction="column" mb={1} ml={1} mr={1}>
          <Typography variant="h4">
            <FormattedMessage id="product_details" />
          </Typography>
          <Breadcrumbs />
          <Tabs
            value={value}
            onChange={handleChange}
            aria-label="basic tabs example"
            sx={{marginBottom: '10px'}}
          >
            <Tab
              label={<FormattedMessage id="specifications" />}
              {...a11yProps(0)}
            />

          </Tabs>
          <TabPanel value={value} index={0}>
            <ProductSpecification />
          </TabPanel>
        </Stack>
      </Container>
    </Page>
  )
}
