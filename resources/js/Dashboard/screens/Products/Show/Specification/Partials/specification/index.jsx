import * as React from "react";
import { useForm, useFieldArray } from "react-hook-form";
import { Button, Grid, Box } from "@mui/material";
import { FormattedMessage } from "react-intl";
import {useMutation, useQueries, useQuery} from 'react-query';
import { useClient } from "@/context/auth-context";
import MultiCheckBoxes from "./multiCheckBoxes";
import { useParams } from "react-router";
import {errorWithCustomMessage} from '@/utils/notifications';

const fieldArrayName = "array";

export default function SpecificationSection({templateId}) {
    const { control, handleSubmit } = useForm();
    const {productId} = useParams()
    const { fields, append, update, remove } = useFieldArray({
        control,
        name: fieldArrayName,
        defaultValues: {
            [fieldArrayName]: [],
        },
    });

    const client = useClient();

    const { mutate, isError, error, isLoading } = useMutation(
        (data) =>
            client(`products/${productId}/variable-product-specifications`, {
                method:'POST',
                data: data,
            }),
    );

    const {data:template} = useQuery(
        ["templates"],
        () => client(`specification-templates/${templateId}`),
        {
            keepPreviousData: true,
            onError: () => {
                errorWithCustomMessage("failed_with_reload_msg");
            },
            enabled:true
        }
    );

    const onSubmit = (data) => {
        let trees = data.array.map((item) => {
            let depth = 0;
            let tree = Object.entries(item).map(([key, value]) => {
                let info = key.split("-#-");
                if (depth < info[0]) depth = info[0];
                return {
                    parentLvl: info[0],
                    name: info[1],
                    id: info[2],
                    selected: value,
                    relation: [],
                    specification_id: template?.data?.data[info[0]],
                    specification_value_id: info[2]

                };
            });
            for (var i = depth; i > 0; i--) {
                for (var j = 0; j < tree.length; j++) {
                    if (tree[j].parentLvl == i && tree[j].selected) {
                        for (var k = 0; k < tree.length; k++) {
                            if (tree[k].parentLvl == i - 1)
                                tree[k].relation.push({...tree[j]
                            });
                        }
                    }
                }
            }
            return tree.filter(
                (node) => node?.parentLvl === "0" && node?.selected
            );
        });
        let finalTree = [];
        trees.map((tree) => {
            tree.map((leaf) => {
                let index = finalTree.map((root) => root.id).indexOf(leaf.id);
                if (index !== -1) {
                    finalTree[index] = compareChild(finalTree[index], leaf);
                } else {
                    finalTree.push(leaf);
                }
            });
        });
        mutate({
            variableProductSpecifications:finalTree
        })
    };

    function compareChild(parent1, parent2) {
        for (var i = 0; i < parent2.relation.length; i++) {
            if (
                parent1.relation.some(
                    (child) => parent2.relation[i].id === child.id
                )
            ) {
                parent1 = compareChild(child, parent2.relation[i]);
            } else {
                parent1.relation.push(parent2.relation[i]);
            }
        }
        return parent1;
    }

    let ids = template?.data?.data ?? []
    const Qs = useQueries(
        ids.map((id) => {
            return {
                queryKey: ["spicification-", id],
                queryFn: () => client(`specifications/${id}`),
            };
        })
    );

    return (
        <Box component="form" onSubmit={handleSubmit(onSubmit)} mt={2} >
            <Grid container spacing={2}>
                {fields.map((field, index) => (
                    <Grid
                        item
                        xs="auto"
                        key={field.id}
                        sx={{ width: "fit-content !important" }}
                    >
                        <Box
                            border="1px solid"
                            sx={{ p: 2 }}
                            borderRadius={2}
                            borderColor="darkgray"
                            width="100%"
                            flexWrap="wrap"
                        >
                            <MultiCheckBoxes
                                specificationsValues={Qs?.map(
                                    (query) => query?.data?.data
                                )}
                                control={control}
                                update={update}
                                index={index}
                                value={field}
                            />
                            <Button
                                type="button"
                                onClick={() => remove(index)}
                                color="error"
                            >
                                <FormattedMessage id="remove" />
                            </Button>
                        </Box>
                    </Grid>
                ))}
                <Grid item md={3} xs={12} alignSelf="center">
                    <Box
                        sx={{
                            border: "1px dashed",
                            display: "grid",
                            alignItems: "center",
                            placeItems: "center",
                            width: "100%",
                        }}
                    >
                        <Button
                            type="button"
                            onClick={() => {
                                append({});
                            }}
                            sx={{
                                width: "100%",
                            }}
                        >
                            +
                        </Button>
                    </Box>
                </Grid>
            </Grid>
            <Button
                type="submit"
                color="success"
                variant="outlined"
                fullWidth
                sx={{ mt: 2 }}
            >
                <FormattedMessage id="submit" />
            </Button>
        </Box>
    );
}
