import { Container } from "@mui/system";
import React from "react";
import DropzoneField from "@/components/Form/components/DropzoneField/index";
import { useForm } from "react-hook-form";
import { Button } from "@mui/material";
import { FormattedMessage } from "react-intl";
import { useMutation } from "react-query";
import { useAuth } from "@/context/auth-context";
import axios from "axios";

const UploaderSection = () => {
    const { control, handleSubmit, setValue, errors } = useForm();
    let product = {};
    const { user: userAuth } = useAuth();

    const { mutate } = useMutation((data) =>
        axios({
            url: `/api/products/${id}`,
            method: "POST",
            data,
            headers: {
                "Content-Type": "multipart/form-data",
                Authorization: userAuth?.token
                    ? `Bearer ${userAuth?.token}`
                    : undefined,
            },
        })
    );

    const onFileChange = async (name, files) => {
        setValue(name, files, { shouldDirty: true });
        if (files?.file) {
            await trigger(["image"]);
        }
    };

    const onSubmit = (data) => {
        mutate(data);
    };

    return (
        <Container sx={{ my: 2 }}>
            <form onSubmit={handleSubmit(onSubmit)}>
                <DropzoneField
                    name="image"
                    control={control}
                    InputChange={(name, files) => onFileChange(name, files)}
                    errors={errors}
                    editValue={product?.imageFileUrl}
                />
                <Button type="submit" fullWidth>
                    <FormattedMessage id="upload_file" />
                </Button>
            </form>
        </Container>
    );
};

export default UploaderSection;
