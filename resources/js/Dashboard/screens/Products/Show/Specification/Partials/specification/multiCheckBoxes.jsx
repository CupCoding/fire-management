import { useForm } from "react-hook-form";
import { Button, Stack, Typography } from "@mui/material";
import { FormattedMessage } from "react-intl";
import CustomCheckbox from "@/components/Form/components/CustomCheckbox";

const MultiCheckBoxes = ({ update, index, value, specificationsValues }) => {
    const { handleSubmit, control } = useForm({
        defaultValues: value,
    });

    return (
        <Stack gap={2} alignItems="start" flexWrap="wrap">
            {specificationsValues.map((specification, index) => {
                return (
                    <Stack key={specification.id}>
                        <Typography component="h3">
                            {specification.name}
                        </Typography>
                        <Stack flexDirection="row" flexWrap="wrap">
                            {specification?.specification_values?.map(
                                (item) => {
                                    return (
                                        <CustomCheckbox
                                            control={control}
                                            name={`${index}-#-${"uu"}-#-${
                                                item.id
                                            }`}
                                            label={item.value}
                                            key={item.id}
                                        />
                                    );
                                }
                            )}
                        </Stack>
                    </Stack>
                );
            })}

            <Button
                type="button"
                onClick={handleSubmit((data) => {
                    update(index, data);
                })}
            >
                <FormattedMessage id="save" />
            </Button>
        </Stack>
    );
};

export default MultiCheckBoxes;
