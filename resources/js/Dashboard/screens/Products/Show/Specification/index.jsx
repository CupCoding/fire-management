import React from "react";
import Page from "@/components/Page";
import { Container} from "@mui/material";
import UploaderSection from "./Partials/UploaderSection";
import SpecificationSection from "./Partials/specification";
import AsyncSelect from '../../../../components/Form/components/AsyncSelect';
import {useForm} from 'react-hook-form';

const ProductSpecification = () => {
    const {
        control,
        watch,
    } = useForm({
        defaultValues: {
            template: "",
        },
    });
    return (
        <Page title="Product Specifications">
            <Container>
                <UploaderSection />
                <form
                    autoComplete="off"
                    noValidate
                    sx={{  }}
                >
                    <AsyncSelect
                        title="template"
                        name="template"
                        optionUrl="specification-templates"
                        control={control}
                        errors={''}
                        optionLabel="title"
                    />
                </form>
                {watch('template')?.id && <SpecificationSection templateId={watch('template')?.id} />}
            </Container>
        </Page>
    );
};

export default ProductSpecification;
