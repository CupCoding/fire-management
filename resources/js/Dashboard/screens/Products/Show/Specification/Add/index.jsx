import React from "react";
import Page from "@/components/Page";
import { Container} from "@mui/material";
import UploaderSection from "../Partials/UploaderSection";
import SpecificationSection from "../Partials/specification";

const ProductSpecification = () => {
    return (
        <Page title="Product Specifications">
            <Container>
                <UploaderSection />
                <SpecificationSection />
            </Container>
        </Page>
    );
};

export default ProductSpecification;
