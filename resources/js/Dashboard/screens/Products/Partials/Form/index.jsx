import CustomInput from "@/components/Form/components/CustomInput";
import CustomTextarea from "@/components/Form/components/CustomTextarea";
import DropzoneField from "@/components/Form/components/DropzoneField";
import { FullPageSpinner } from "@/components/lib";
import { useAuth, useClient } from "@/context/auth-context";
import { getErrorsFromResponse } from "@/utils/fromHelper";
import { successWithCustomMessage } from "@/utils/notifications";
import { getRouteWithLang } from "@/utils/routesHelpers";
import { yupResolver } from "@hookform/resolvers/yup";
import { Stack } from "@mui/material";
import axios from "axios";
import React, { useEffect } from "react";
import { useForm } from "react-hook-form";
import { useMutation, useQuery, useQueryClient } from "react-query";
import { useNavigate, useParams } from "react-router-dom";
import * as Yup from "yup";
import AsyncSelect from '@/components/Form/components/AsyncSelect';
import SubmitLayout from '@/components/SubmitLayout';
import ErrorAlert from "@/components/ErrorAlert";

export default function Form() {
    const { productId } = useParams();
    const client = useClient();
    const queryClient = useQueryClient();
    const { user: userAuth } = useAuth();
    const navigate = useNavigate();
    const productsRoute = getRouteWithLang("/products");
    const [backendErrors, setBackendErrors] = React.useState([]);

    const Schema = Yup.object().shape({
        name_ar: Yup.string().required("name_ar_is_required"),
        name_en: Yup.string().required("name_en_is_required"),
        description_ar: Yup.string().required("description_ar_is_required"),
        description_en: Yup.string().required("description_en_is_required"),
        brand: Yup.object().typeError('brand_is_required').required('brand_is_required'),
        category: Yup.object().typeError('category_is_required').required('category_is_required'),
        image: Yup.mixed().required('image_is_required'),
    });

    const { isLoading: fetchLoading, data: product } = useQuery({
        queryKey: `product_${productId}`,
        queryFn: () => client(`products/${productId}`).then((data) => data),
        enabled: productId !== undefined,
    });

    const {
        control,
        handleSubmit,
        reset,
        trigger,
        setValue,
        formState: { errors, isDirty },
    } = useForm({
        resolver: yupResolver(Schema),
        defaultValues: {
            name_ar: "",
            name_en: "",
            description_ar: "",
            description_en: "",
            image: undefined,
            category:[],
            brand:[]
        },
    });

    const onFileChange = async (name, files) => {
        setValue(name, files, { shouldDirty: true });
        if (files?.file) {
            await trigger(["image"]);
        }
    };
    useEffect(() => {
        if (product && productId !== undefined) {
            let ReceivedProduct = {}
            product?.data?.translations?.map(lang =>{
                if(lang.locale === 'ar'){
                    ReceivedProduct.name_ar = lang.name
                    ReceivedProduct.description_ar = lang.description
                }
                else{
                    ReceivedProduct.name_en = lang.name
                    ReceivedProduct.description_en = lang.description
                }
            })
            reset({
                ...ReceivedProduct
            })
        }
    }, [product]);

    const {mutate, isError, isLoading} = useMutation(
        (data) =>
        axios({
            url: productId ? `/api/admin/products/${productId}` : `/api/admin/products`,
            method: "POST",
            data,
            headers: {
                "Content-Type": "multipart/form-data",
                Authorization: userAuth?.token
                    ? `Bearer ${userAuth?.token}`
                    : undefined,
            },
        }),
        {
          onSuccess: () => {
            queryClient.invalidateQueries('products')
            navigate(`${productsRoute}`)
            reset()
            if (productId) successWithCustomMessage('updated_success_msg')
            else successWithCustomMessage('added_success_msg')
          },
          onError: error => {
            let errors = getErrorsFromResponse(error)
            setBackendErrors(errors)
          },
        },
      )
      const onSubmitForm = ({
        name_ar,
        name_en,
        description_ar,
        description_en,
        image,
        category,
        brand
    }) => {
        const formData = new FormData();
        formData.append("brand_id", brand?.id);
        formData.append("category_id", category?.id);
        formData.append("ar[name]", name_ar);
        formData.append("en[name]", name_en);
        formData.append("ar[description]", description_ar);
        formData.append("en[description]", description_en);
        !image
            ? formData.append("image", null)
            : formData.append("image", image);
        productId && formData.append("_method", "PUT");
        mutate(formData);
    };


    if (fetchLoading) {
        return <FullPageSpinner />;
    }

    return (
        <>
            <form
                autoComplete="off"
                noValidate
                onSubmit={handleSubmit(onSubmitForm)}
            >
                <Stack spacing={3}>
                    <ErrorAlert isError={isError} errors={backendErrors} />

                    <CustomInput
                        label="name_ar"
                        name="name_ar"
                        control={control}
                        errors={errors}
                    />
                    <CustomInput
                        label="name_en"
                        name="name_en"
                        control={control}
                        errors={errors}
                    />
                     <CustomTextarea
                        label="description_ar"
                        name="description_ar"
                        control={control}
                        errors={errors}
                    />
                    <CustomTextarea
                        label="description_en"
                        name="description_en"
                        control={control}
                        errors={errors}
                    />
                    <AsyncSelect
                        title="brand"
                        name="brand"
                        optionUrl="brands"
                        control={control}
                        errors={errors}
                        optionLabel="name"
                    />
                    <AsyncSelect
                        title="category"
                        name="category"
                        optionUrl="categories"
                        control={control}
                        errors={errors}
                        optionLabel="name"
                    />

                    <DropzoneField
                        name="image"
                        control={control}
                        InputChange={(name, files) => onFileChange(name, files)}
                        errors={errors}
                        editValue={product?.data?.image}
                    />
                </Stack>
                <SubmitLayout
                    isLoading={isLoading}
                    isDisabled={!isDirty}
                    label={id !== undefined ? 'update' : 'save'}
                    cancelAction={()=> navigate(-1)}
                />
            </form>
        </>
    );
}
