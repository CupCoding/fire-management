import CustomInput from "@/components/Form/components/CustomInput";
import CustomTextarea from "@/components/Form/components/CustomTextarea";
import DropzoneField from "@/components/Form/components/DropzoneField";
import { FullPageSpinner } from "@/components/lib";
import { useClient } from "@/context/auth-context";
import { getErrorsFromResponse } from "@/utils/fromHelper";
import { successWithCustomMessage } from "@/utils/notifications";
import { getRouteWithLang } from "@/utils/routesHelpers";
import { yupResolver } from "@hookform/resolvers/yup";
import { Stack } from "@mui/material";
import React, { useEffect } from "react";
import { useForm } from "react-hook-form";
import { useMutation, useQuery, useQueryClient } from "react-query";
import { useNavigate, useParams } from "react-router-dom";
import * as Yup from "yup";
import SubmitLayout from "@/components/SubmitLayout";
import ErrorAlert from "@/components/ErrorAlert";
import { prepareDataForm } from '@/utils/data-managment';
import { readTranslationFromResponse } from "@/utils/data-managment";

export default function Form() {
    const { id } = useParams();
    const client = useClient('multipart/form-data');
    const queryClient = useQueryClient();
    const navigate = useNavigate();
    const brandsRoute = getRouteWithLang("/brands");
    const [backendErrors, setBackendErrors] = React.useState([]);

    const Schema = Yup.object().shape({
        name_ar: Yup.string().required("field_is_required"),
        name_en: Yup.string().required("field_is_required"),
        description_ar: Yup.string().required("field_is_required"),
        description_en: Yup.string().required("field_is_required"),
        image: Yup.mixed().nullable(),
    });

    const { isLoading: fetchLoading, data: brand } = useQuery({
        queryKey: `brand_${id}`,
        queryFn: () => client(`brands/${id}`).then((data) => data.data),
        enabled: id !== undefined,
    });

    const {
        control,
        handleSubmit,
        reset,
        trigger,
        setValue,
        formState: { errors, isDirty },
    } = useForm({
        resolver: yupResolver(Schema),
        defaultValues: {
            name_ar: "",
            name_en: "",
            description_ar: "",
            description_en: "",
            image: "",
        },
    });

    const onFileChange = async (name, files) => {
        setValue(name, files, { shouldDirty: true });
        if (files?.file) {
            await trigger(["image"]);
        }
    };

    useEffect(() => {
        if (brand && id !== undefined) {
            reset({
                ...readTranslationFromResponse(brand)
            })
        }
    }, [brand]);

    const { mutate, isError, isLoading } = useMutation(
        (data) =>
            client(`${id ? `brands/${id}` : 'brands'} `, {
            method: 'POST',
            data,
            }),
        {
            onSuccess: () => {
                queryClient.invalidateQueries("brands");
                navigate(`${brandsRoute}`);
                reset();
                id !== null ? successWithCustomMessage("updated_success_msg") : successWithCustomMessage("added_success_msg");
            },
            onError: (error) => {
                let errors = getErrorsFromResponse(error);
                setBackendErrors(errors);
            },
        }
    );

    const onSubmitForm = (data) => {
        let formData = prepareDataForm(data)
        !data.image
            ? formData.append("image", null)
            : formData.append("image", data.image);
        id && formData.append("_method", "PUT");
        mutate(formData);
    };

    if (fetchLoading) {
        return <FullPageSpinner />;
    }

    return (
        <>
            <form
                autoComplete="off"
                noValidate
                onSubmit={handleSubmit(onSubmitForm)}
            >
                <Stack spacing={3}>
                    <ErrorAlert isError={isError} errors={backendErrors} />

                    <CustomInput
                        label="name_ar"
                        name="name_ar"
                        control={control}
                        errors={errors}
                    />
                    <CustomInput
                        label="name_en"
                        name="name_en"
                        control={control}
                        errors={errors}
                    />
                    <CustomTextarea
                        label="description_ar"
                        name="description_ar"
                        control={control}
                        errors={errors}
                    />
                    <CustomTextarea
                        label="description_en"
                        name="description_en"
                        control={control}
                        errors={errors}
                    />

                    <DropzoneField
                        name="image"
                        control={control}
                        InputChange={(name, files) => onFileChange(name, files)}
                        errors={errors}
                        editValue={brand?.data?.image}
                    />
                </Stack>
                <SubmitLayout
                    isLoading={isLoading}
                    isDisabled={!isDirty}
                    label={id !== undefined ? 'update_brand' : 'save'}
                    cancelAction={()=> navigate(-1)}
                />
            </form>
        </>
    );
}
