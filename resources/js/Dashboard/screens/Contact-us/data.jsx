import React from "react";
import MoreMenu from "./Partials/MoreMenu";

export const tableColumns = [
    {
        header: "name",
        accessorKey: "name",
    },
    {
        header: "email",
        accessorKey: "email",
        enableSorting: false,
    },
    {
        header: "subject",
        accessorKey: "subject",
        enableSorting: false,
    },
    {
        header: "actions",
        accessorKey: "actions",
        cell: ({ row }) => (<MoreMenu id={row.original.id}/>),
        enableSorting: false,
    },
];
