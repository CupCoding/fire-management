import CustomInput from "@/components/Form/components/CustomInput";
import { FullPageSpinner } from "@/components/lib";
import { useClient } from "@/context/auth-context";
import { getErrorsFromResponse } from "@/utils/fromHelper";
import { successWithCustomMessage } from "@/utils/notifications";
import { getRouteWithLang } from "@/utils/routesHelpers";
import { yupResolver } from "@hookform/resolvers/yup";
import { Stack } from "@mui/material";
import React, { useEffect } from "react";
import { useForm } from "react-hook-form";
import { useMutation, useQuery, useQueryClient } from "react-query";
import { useNavigate, useParams } from "react-router-dom";
import * as Yup from "yup";
import AsyncSelect from '../../../../components/Form/components/AsyncSelect';
import SubmitLayout from '@/components/SubmitLayout';
import ErrorAlert from "@/components/ErrorAlert";

export default function Form() {
    const { id } = useParams();
    const client = useClient();
    const queryClient = useQueryClient();
    const navigate = useNavigate();
    const rolesRoute = getRouteWithLang("/roles");
    const [backendErrors, setBackendErrors] = React.useState([]);

    const Schema = Yup.object().shape({
        name: Yup.string().required("field_is_required"),
        display_name: Yup.string().required("field_is_required"),
        permissions: Yup.array().required("field_is_required"),
    });

    const { isLoading: fetchLoading, data: role } = useQuery({
        queryKey: `role_${id}`,
        queryFn: () => client(`roles/${id}`).then((data) => data.data),
        enabled: id !== undefined,
    });

    const {
        control,
        handleSubmit,
        reset,
        formState: { errors, isDirty },
    } = useForm({
        resolver: yupResolver(Schema),
        defaultValues: {
            name: "",
            display_name: "",
            permissions: [],
        },
    });

    useEffect(() => {
        if (role && id !== undefined) {
            reset({
                ...role
            })
        }
    }, [role]);

    const { mutate, isError, isLoading } = useMutation(
        data =>
          client(`${id ? `roles/${id}` : 'roles'} `, {
            method: id ? 'PUT' : 'POST',
            data,
          }),
        {
            onSuccess: () => {
                queryClient.invalidateQueries("roles");
                navigate(`${rolesRoute}`);
                reset();
                if (id) successWithCustomMessage("updated_success_msg");
                else successWithCustomMessage("added_success_msg");
            },
            onError: (error) => {
                let errors = getErrorsFromResponse(error);
                setBackendErrors(errors);
            },
        }
    );

    const onSubmitForm = ({
        name,
        display_name,
        permissions,
    }) => {
        mutate({name,permissions:permissions.map(permission => permission.id),display_name});
    };

    if (fetchLoading) {
        return <FullPageSpinner />;
    }

    return (
        <>
            <form
                autoComplete="off"
                noValidate
                onSubmit={handleSubmit(onSubmitForm)}
            >
                <Stack spacing={3}>
                    <ErrorAlert isError={isError} errors={backendErrors} />

                    <CustomInput
                        label="name"
                        name="name"
                        control={control}
                        errors={errors}
                    />
                    <CustomInput
                        label="job_title"
                        name="display_name"
                        control={control}
                        errors={errors}
                    />
                    <AsyncSelect
                        title="permissions"
                        name="permissions"
                        optionUrl="permissions"
                        control={control}
                        errors={errors}
                        optionLabel="name"
                        multiple={true}
                    />
                </Stack>
                <SubmitLayout
                    isLoading={isLoading}
                    isDisabled={!isDirty}
                    label={id !== undefined ? 'update' : 'save'}
                    cancelAction={()=> navigate(-1)}
                />
            </form>
        </>
    );
}
