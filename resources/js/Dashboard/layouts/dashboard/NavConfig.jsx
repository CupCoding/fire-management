// component
import Iconify from "../../components/Iconify";
import * as React from "react";

const getIcon = (name) => <Iconify icon={name} width={22} height={22} />;

const navConfig = [
    {
        title: "brands",
        path: "/brands",
        icon: getIcon("mdi:alpha-b-box-outline")
    },
    {
        title: "categories",
        path: "/categories",
        icon: getIcon("carbon:categories"),
    },
    {
        title: "contact-us",
        path: "/contact-us",
        icon: getIcon("material-symbols:connect-without-contact"),
    },
    {
        title: "countries",
        path: "/countries",
        icon: getIcon("uiw:global"),
    },
    {
        title: "currencies",
        path: "/currencies",
        icon: getIcon("material-symbols:currency-exchange"),
    },
    {
        title: "faqs",
        path: "/faqs",
        icon: getIcon("wpf:faq"),
    },
    {
        title: "products",
        path: "/products",
        icon: getIcon("bi:boxes"),
    },
    {
        title:"reviews",
        path:"/reviews",
        icon: getIcon('uis:star-half-alt')
    },
    {
        title:"roles",
        path:"/roles",
        icon: getIcon('mdi:account-security-outline')
    },
    {

        title: "specifications",
        path: "/specifications",
        icon: getIcon("eva:shopping-bag-fill"),
    },

    {
        title:'templates',
        path:'/templates',
        icon: getIcon('mdi:paper-add-outline')
    },
    {
        title:'test',
        path:'/test',
        icon: getIcon('carbon:test-tool')
    },
    {
        title: "users",
        path: "/users",
        icon: getIcon("eva:people-fill"),
    },
];

export default navConfig;
