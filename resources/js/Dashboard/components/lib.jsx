import { FormattedMessage } from 'react-intl';
import LoadingBlocks from './LoadingBlocks/index';

function FullPageSpinner() {
    return (
        <div
            css={{
                fontSize: "4em",
                height: "100vh",
                display: "flex",
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "center",
                color: "#1890FF",
            }}
        >
            <LoadingBlocks />
        </div>
    );
}

function ModalSpinner() {
    return (
        <div
            css={{
                fontSize: "4em",
                display: "flex",
                height: "30vh",
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "center",
                color: "#1890FF",
            }}
        >
            <LoadingBlocks />
        </div>
    );
}

function FullPageErrorFallback() {
    return (
        <div
            role="alert"
            css={{
                height: "100vh",
                display: "flex",
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "center",
            }}
        >
            <p>
                <FormattedMessage id="full_page_error" />
            </p>
        </div>
    );
}

export { FullPageErrorFallback, FullPageSpinner, ModalSpinner };
