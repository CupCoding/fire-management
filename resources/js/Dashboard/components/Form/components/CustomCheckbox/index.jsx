import { Checkbox, FormControlLabel } from "@mui/material";
import React from "react";
import { Controller } from "react-hook-form";
import { FormattedMessage } from "react-intl";

export default function CustomCheckbox({ label, control, name }) {
    function generateLabel(){
        try{
            return <FormattedMessage id={label} defaultMessage={label} />
        }catch(err){
            return label
        }
    }
    return (
        <Controller
            name={name}
            control={control}
            render={({ field }) => {
                return (
                    <FormControlLabel
                        control={
                            <Checkbox
                                onChange={(e) =>
                                    field.onChange(e.target.checked)
                                }
                                checked={field.value}
                            />
                        }
                        label={generateLabel()}
                    />
                );
            }}
        />
    );
}
