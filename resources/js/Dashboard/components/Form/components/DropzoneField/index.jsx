import { Box, IconButton, TextField, Typography } from "@mui/material";
import React, { useEffect, useState } from "react";
import { useDropzone } from "react-dropzone";
import { Controller } from "react-hook-form";
import { FormattedMessage } from "react-intl";
import Iconify from "../../../Iconify";
import { styled } from "@mui/material/styles";

const StyledBox = styled(Box)(({ theme }) => ({
    border: `2px dashed ${theme.palette.primary.main}`,
    borderRadius: theme.shape.borderRadius,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    background: theme.palette.background.default,
    height: "8rem",
    outline: "none",
    justifyItems: "center",
}));

const ThumbContainer = styled(Box)(({ theme }) => ({
    display: "flex",
    flexDirection: "row",
    flexWrap: "wrap",
    marginTop: 16,
}));

const Thumb = styled(Box)(({ theme }) => ({
    display: "inline-flex",
    borderRadius: 4,
    border: "1px solid grey",
    marginBottom: 8,
    marginRight: 8,
    width: 100,
    height: 100,
    padding: 4,
    boxSizing: "border-box",
}));

const ThumbInner = styled(Box)(({ theme }) => ({
    display: "flex",
    minWidth: 0,
    overflow: "hidden",
    position: "relative",
}));

const ThumbImg = styled(Box)(({ theme }) => ({
    display: "block",
    width: "auto",
    height: "100%",
}));

const ThumbDeleteButton = styled(IconButton)(({ theme }) => ({
    position: "absolute",
    left: theme.direction === "ltr" ? 2.5 : "unset",
    right: theme.direction === "rtl" ? 2.5 : "unset",
    bottom: 2.5,
    background: theme.palette.error.main,
    color: "#fff",
    border: 0,
    borderRadius: ".325em",
    cursor: "pointer",
    width: "1.6rem",
    height: "1.6rem",
    display: "flex",
}));

const ErrorText = styled("div")(({ theme }) => ({
    color: "#f44336",
    marginLeft: "14px",
    marginRight: "14px",
    margin: "0",
    fontSize: "0.75rem",
    marginTop: "3px",
    fontWeight: "400",
    lineHeight: "1.66",
    letterSpacing: "0.03333em",
}));

const DropzoneField = ({
    name,
    control,
    InputChange,
    hidden = false,
    thumbActions = true,
    maxSize = 2097152,
    errors,
    editValue,
    onDropAccepted = () => {},
    accept,
    ...rest
}) => {
    const [files, setFiles] = useState([]);
    return (
        <Controller
            render={({ field: { onChange }, fieldState: { error } }) => (
                <Dropzone
                    onChange={(e) => onChange(e.target.files[0])}
                    setFiles={setFiles}
                    files={files}
                    hidden={hidden}
                    thumbActions={thumbActions}
                    maxSize={maxSize}
                    errors={errors}
                    name={name}
                    editValue={editValue}
                    InputChange={InputChange}
                    error={error}
                    onDropAccepted={onDropAccepted}
                    accept={accept}
                    {...rest}
                />
            )}
            name={name}
            control={control}
        />
    );
};

const Dropzone = ({
    onChange,
    setFiles,
    files,
    hidden,
    thumbActions,
    maxSize,
    errors,
    name,
    editValue,
    onDropAccepted,
    InputChange,
    error,
    accept,
    ...rest
}) => {
    const fileSizeValidator = (file) => {
        if (file.length > maxSize)
            return {
                code: "file-too-large",
                message: `Max file size is ${maxSize / 2048} MB`,
            };
    };
    const { getRootProps, getInputProps, fileRejections } = useDropzone({
        ...rest,
        onDrop: (acceptedFiles) => {
            acceptedFiles.map((file) => {
                if (!files.some((f) => f.path === file.path)) {
                    Object.assign(file, {
                        preview: URL.createObjectURL(file),
                    });
                    setFiles((prev) => [...prev, file]);
                }
            });
        },
        onDropAccepted: (file) => {
            onDropAccepted && onDropAccepted();
            InputChange && InputChange(name,file);
        },
        maxSize,
        validator: fileSizeValidator,
        onDropRejected: () => {
            setFiles([]);
        },
        accept: accept
            ? accept
            : {
                  "image/*": [".jpeg", ".png"],
              },
    });

    return (
        <>
            <Box sx={{ display: hidden ? "none" : "block" , cursor:'pointer' }}>
                <StyledBox {...getRootProps()}>
                    <TextField
                        type="file"
                        {...getInputProps({ onChange })}
                        accept="image/*"
                        hidden={hidden}
                        sx={{ width: "100%", height: "100%" }}
                    />
                    <Typography sx={{ color: "grey", fontSize: "13px" }}>
                        <FormattedMessage id="drag_and_drop_some_files_here_or_click_here_to_select_files" />
                    </Typography>
                </StyledBox>
                {files !== null && (
                    <ThumbContainer>
                        {files.map((file) => (
                            <Thumb key={file && file.name}>
                                <ThumbInner>
                                    <ThumbImg
                                        component="img"
                                        src={file?.preview}
                                    />
                                    {thumbActions && (
                                        <ThumbDeleteButton
                                            color="info"
                                            onClick={() =>setFiles(() =>files.filter((f) =>file.path !==f.path))}
                                            >
                                            <Iconify
                                                width={"0.5 rem"}
                                                height={"0.5 rem"}
                                                icon="ic:twotone-delete-outline"
                                                />
                                        </ThumbDeleteButton>
                                    )}
                                </ThumbInner>
                            </Thumb>
                        ))}
                    </ThumbContainer>
                )}
                <ErrorText>
                    {error && <FormattedMessage id={error?.message} />}
                    {fileRejections.length > 0 &&
                        `Max file size is ${(maxSize / 1048576).toFixed(2)} MB`}
                </ErrorText>
            </Box>
            <ErrorText
                sx={{ display: hidden ? "block" : "none", marginTop: "220px" }}
            >
                {fileRejections.length > 0 &&
                    `Max file size is ${(maxSize / 1048576).toFixed(2)} MB`}
            </ErrorText>
        </>
    );
};

export default DropzoneField;
