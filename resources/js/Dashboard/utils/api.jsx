import { QueryCache } from "react-query";
import * as auth from "../auth-provider";
const apiURL = `${window.location.origin}/api/admin`;
import axios from "axios";
import { accessIntoPaginatedDataFromResponse } from "./data-managment";

async function client(
    endpoint,
    { data, token, headers: customHeaders, ...customConfig } = {}
) {
    const queryCache = new QueryCache();
    const config = {
        url: `${apiURL}/${endpoint}`,
        method: data ? "POST" : "GET",
        data: data ? data : undefined,
        headers: {
            Authorization: token ? `Bearer ${token}` : undefined,
            "Content-Type": data ? "application/json" : undefined,
            ...customHeaders,
        },
        ...customConfig,
    };

    return axios(config).then(async (response) => {
        if (response.status === 401 || response.status === 403) {
            queryCache.clear();
            await auth.logout();
            // refresh the page for them
            window.location.assign(window.location);
            return Promise.reject({ message: "Please re-authenticate." });
        }
        const data = await response;
        if (response.status === 200) {
            return accessIntoPaginatedDataFromResponse(data);
        } else {
            return Promise.reject(data);
        }
    }).catch(async({response}) => {
        if (response.status === 401 || response.status === 403) {
            queryCache.clear();
            await auth.logout();
            // refresh the page for them
            window.location.assign(window.location);
            return Promise.reject({ message: "Please re-authenticate." });
        }
        return Promise.reject(response)
    });
}

export { client };
