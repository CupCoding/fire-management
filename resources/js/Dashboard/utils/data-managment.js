export const accessIntoPaginatedDataFromResponse = (data) => {
    return {
        data: data?.data?.content,
        meta: {
            pages: data?.data?.paginator?.total_page,
            page: data?.data?.paginator?.current_page,
        },
    };
};

export function prepareDataForm(data) {
    const formData = new FormData();
    Object.entries(data).map(([key, value]) => {
        if (key.includes('_ar')) {
            formData.append(`ar[${key.substring(0, key.length - 3)}]`, value);
        }
        else if (key.includes('_en')) {
            formData.append(`en[${key.substring(0, key.length - 3)}]`, value);
        }
        else {
            formData.append(key, value)
        }
    });

    return formData;
}

export function readTranslationFromResponse(data) {
    let responseData = {}
    Object.entries(data).map(([key, value]) => {
        if (Array.isArray(value)) {
            value.map(lang => {
                if (lang.locale === 'ar') {
                    Object.entries(lang).map(([key, value]) => {
                        responseData[`${key}_ar`] = value
                    });
                }
                else {
                    Object.entries(lang).map(([key, value]) => {
                        responseData[`${key}_en`] = value
                    });
                }
            })
        }
        else
            responseData[key] = value
    });
    return responseData
}
