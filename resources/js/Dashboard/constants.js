export const supportedLanguages = ['en', 'ar']
export const defaultLang = 'en'
export const dateTimeFormat = 'dd/MM/yyyy hh:mm '
