<?php

namespace App\Repositories;

use App\Http\QueryFilter\QueryFilter;

abstract class BaseRepository
{
    public function findAll(QueryFilter $filter, $entityClassName)
    {
        return $entityClassName::query()->filter($filter);
    }
}

