<?php

namespace App\Repositories;

use App\Http\QueryFilter\QueryFilter;
use App\Models\Center;

class CarRepository extends BaseRepository
{
    public function findAll(QueryFilter $queryFilter, $entityClassName)
    {
        return Center::query()
            ->select('cars.*')
            ->filter($queryFilter);
    }
}
