<?php

namespace App\Repositories;

use App\Http\QueryFilter\QueryFilter;
use App\Models\Center;

class CenterRepository extends BaseRepository
{
    public function findAll(QueryFilter $queryFilter, $entityClassName)
    {
        return Center::query()
            ->select('centers.*')
            ->filter($queryFilter);
    }
}
