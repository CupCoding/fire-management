<?php

namespace App\Service;

use App\Models\Center;
use App\Repositories\CenterRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class CenterService extends BaseService
{
    public function __construct(CenterRepository $centerRepository)
    {
        parent::__construct($centerRepository, Center::class);
    }

    public function add($validatedData, $image = null): Center
    {
        try {
            DB::beginTransaction();
            $center = new Center($validatedData);

            if ($image)
                $center->image = Storage::disk('public')->put('center', $image);

            $center->save();
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
        return $center;
    }

    public function update($validatedData, $center, $image = null)
    {
        try {
            DB::beginTransaction();
            $center->update($validatedData);

            if ($image) {
                // if there is an old avatar delete it
                if ($center->image != null && Storage::disk('public')->exists($center->image)) {
                    $center->image = Storage::disk('public')->delete($center->image);
                }

                // store the new image
                $center->image = Storage::disk('public')->put('brands', $image);
            }

            $center->save();
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
        return $center;
    }

}
