<?php

namespace App\Service;


use App\Http\QueryFilter\QueryFilter;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

abstract class BaseService
{
    protected BaseRepository $entityRepository;
    private string $entityClassName;

    public function __construct(BaseRepository $entityRepository, string $entityClassName)
    {
        $this->entityRepository = $entityRepository;
        $this->entityClassName = $entityClassName;
    }

    public function add($validatedData, $file = null)
    {
        try {
            DB::beginTransaction();
            $baseEntity = new $this->entityClassName($validatedData);
            $baseEntity->save();
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
        return $baseEntity;
    }

    public function update($validatedData, $baseEntity, $file = null)
    {
        try {
            DB::beginTransaction();
            $baseEntity->update($validatedData);
            $baseEntity->save();
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
        return $baseEntity;
    }

    public function delete($baseEntity)
    {
        try {
            DB::beginTransaction();
            $baseEntity->delete();
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    public function findAll(QueryFilter $filter)
    {
        return $this->entityRepository->findAll($filter, $this->entityClassName);
    }
}
