<?php

namespace App\Service;

use App\Models\Role;
use App\Repositories\RoleRepository;

class RoleService extends BaseService
{
    public function __construct(RoleRepository $countryRepository)
    {
        parent::__construct($countryRepository, Role::class);
    }

    public function add($validatedData, $image = null)
    {
        $role = new Role($validatedData);
        $role->guard_name = 'api';

        $role->save();
        $role->syncPermissions($validatedData['permissions']);

        return $role;
    }

    public function update($validatedData, $role, $image = null)
    {
        $role->update($validatedData);
        $role->save();
        if (isset($validatedData['permissions']))
            $role->syncPermissions($validatedData['permissions']);

        return $role;
    }
}
