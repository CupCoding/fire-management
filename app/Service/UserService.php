<?php

namespace App\Service;


use App\Http\QueryFilter\Models\Dashboard\UserFilter;
use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class UserService
{
    /**
     * @throws \Exception
     */
    public function add($validatedData, $avatar): User
    {
        try {
            DB::beginTransaction();
            $user = new User($validatedData);

            if ($validatedData['password'])
                $user->password = bcrypt($validatedData['password']);

            if ($avatar)
                $user->avatar = Storage::disk('public')->put('users', $avatar);

            $user->save();
            if ($validatedData['roles']) {
                $user->syncRoles($validatedData['roles']);
            } else {
                $user->syncRoles(['guest']);
                DB::commit();
            }
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
        return $user;
    }

    public function update($validatedData, User $user, $avatar): User
    {
        try {
            DB::beginTransaction();
            if (isset($validatedData['password']))
                $validatedData['password'] = bcrypt($validatedData['password']);

            $user->update($validatedData);

            if ($avatar) {
                // if there is an old avatar delete it
                if ($user->avatar != null) {
                    $user->avatar = Storage::disk('public')->delete($user->avatar);
                }

                // store the new image
                $user->avatar = Storage::disk('public')->put('users', $avatar);
            }

            $user->save();

            if (isset($validatedData['roles'])) {
                $user->syncRoles($validatedData['roles']);
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
        return $user;
    }

    public function delete(User $user)
    {
        try {
            DB::beginTransaction();
            if ($user->image != null)
                $user->image = Storage::disk('public')->delete($user->image);

            $user->delete();
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    public function getUsers(UserFilter $userFilter)
    {
        return User::filter($userFilter);
    }

    public function updateProfile($validatedData, User $user, $avatar): User
    {
        try {
            DB::beginTransaction();
            if (isset($validatedData['new_password']))
                $user->password = bcrypt($validatedData['new_password']);

            $user->update($validatedData);

            if ($avatar) {
                // if there is an old avatar delete it
                if ($user->avatar != null) {
                    $user->avatar = Storage::disk('public')->delete($user->avatar);
                }

                // store the new image
                $user->avatar = Storage::disk('public')->put('users', $avatar);
            }

            $user->save();
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }

        return $user;
    }

    public function changePassword($validatedData, User $user): User
    {
        try {
            DB::beginTransaction();
            $user->password = bcrypt($validatedData['new_password']);

            $user->update($validatedData);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
        return $user;
    }

    public function checkUser(User $user): bool
    {
        $user = auth()->user();
        if ($user->email === null or $user->name === null or $user->password === null) {
            return false;
        }
        return true;
    }
}
