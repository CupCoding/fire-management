<?php

namespace App\Service;

use App\Models\Car;
use App\Repositories\CenterRepository;
use Illuminate\Support\Facades\DB;

class CarService extends BaseService
{
    public function __construct(CenterRepository $centerRepository)
    {
        parent::__construct($centerRepository, Car::class);
    }

    public function add($validatedData, $file = null): Car
    {
        try {
            DB::beginTransaction();
            $car = new Car($validatedData);

            $car->save();
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
        return $car;
    }

    public function update($validatedData, $car, $file = null)
    {
        try {
            DB::beginTransaction();
            $car->update($validatedData);

            $car->save();
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
        return $car;
    }
}
