<?php
namespace App\Enum;


enum UserType:string
{
    case ADMIN = '1';
    case CUSTOMER = '2';
}
