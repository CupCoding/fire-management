<?php
namespace App\Enum;


enum CenterStatusType:string
{
    case OPEN = '1';
    case CLOSE = '2';
}
