<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Request;

class RefreshTokenMobile
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (!$request->hasHeader('Authorization')) {
            throw new AuthenticationException();
        }
        $token = $request->header('Authorization');
        if (str_contains($token, 'Bearer ')) {
            $token = explode('Bearer ', $request->header('Authorization'))[1];
        }

        if (str_contains($token, '|')) {
            $token = explode('|', $token)[1];
        }

        $user = User::query()->where('token', 'LIKE', '%|' . $token)->first();

        if (!$user || !$request->hasHeader('refresh-token')) {
            throw new AuthenticationException();
        }
        $accessToken = $user->tokens()->orderBy('id', 'DESC')->first();

        if (
            $user->refresh_token != $request->header('refresh-token') ||
            !str_contains($accessToken, User::ABILITIES['customer'])
        ) {
            throw new AuthenticationException();
        }


        $request->user = $user;

        return $next($request);
    }
}
