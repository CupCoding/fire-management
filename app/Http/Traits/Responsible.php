<?php

namespace App\Http\Traits;

use Illuminate\Http\JsonResponse;
use Illuminate\Pagination\LengthAwarePaginator;

trait Responsible
{
    public function respondSuccess($content = [], $paginator = []): JsonResponse
    {
        $res = [
            'result' => true,
            'status_code' => 200,
            'content' => $content,
        ];

        if ($paginator instanceof LengthAwarePaginator)
            $res['paginator'] = [
                'total_count' => $paginator->total(),
                'limit' => $paginator->perPage(),
                'total_page' => ceil($paginator->total() / $paginator->perPage()),
                'current_page' => $paginator->currentPage(),
            ];


        return response()->json($res, 200);
    }

    public function respondError($message, $status = 400): JsonResponse
    {
        return response()->json([
            'result' => false,
            'status_code' => $status,
            'message' => $message,
        ], $status);
    }

    public function respondGeneralError($message, $status = 400): JsonResponse
    {
        return response()->json([
            'result' => false,
            'status_code' => $status,
            'message' => [
                'general' => [$message]
            ],
        ], $status);
    }

}
