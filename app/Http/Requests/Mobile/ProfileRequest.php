<?php

namespace App\Http\Requests\Mobile;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class ProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $user = Auth::user();
        switch ($this->method()) {
            case 'GET':
            case 'DELETE':
                return [];
            case 'POST':
                return [
                    'name' => 'required|max:100',
                    'email' => 'required|email|unique:users,email,' . $user->email,
                    'check_password' => [
                        'max:100',
                        Rule::requiredIf(function () use ($user) {
                            return $user->password !== null;
                        }),
                        function ($attribute, $value, $fail) use ($user) {
                            if (!Hash::check($value, $user->password)) {
                                $fail('Invalid Password.');
                            }
                        }
                    ],
                    'new_password' => [
                        'max:100',
                        'confirmed',
                        Rule::requiredIf(function () use ($user) {
                            return $user->password === null;
                        }),
                    ],
                ];
            case 'PUT':
            case 'PATCH':
            default:
                break;
        }
        return [];

    }
}
