<?php

namespace App\Http\Requests\Mobile;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class ChangePasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $user = Auth::user();
        switch ($this->method()) {
            case 'GET':
            case 'DELETE':
                return [];
            case 'POST':
                return [
                    'current_password' => [
                        'required',
                        'max:100',
                        function ($attribute, $value, $fail) use ($user) {
                            if (!Hash::check($value, $user->password)) {
                                $fail('Invalid Password.');
                            }
                        }
                    ],
                    'new_password' => [
                        'required',
                        'max:100',
                        'confirmed',
                        function ($attribute, $value, $fail) {
                            if (isset($this->validationData()['current_password'])) {
                                if ($this->validationData()['current_password'] === $value) {
                                    $fail('Your new password must be different from your old password. Please try again.');
                                }
                            }
                        }
                    ],
                ];
            case 'PUT':
            case 'PATCH':
            default:
                break;
        }
        return [];

    }
}
