<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        switch ($this->method()) {
            case 'GET':
            case 'DELETE':
                return [];
            case 'POST':
                return [
                    'name' => 'required|max:100',
                    'email' => 'required|email|unique:users',
                    'phone_number' => 'required|min:10',
                    'password' => 'required|confirmed|min:6',
                    'block' => 'bool',
                    'roles.*' => 'required|exists:roles,id',
                    'user_type' => 'required|numeric|in:1,2'
                ];
            case 'PUT':
            case 'PATCH':
                $user = $this->route()->user;
                return [
                    'name' => 'max:100',
                    'email' => 'email|unique:users,email,' . $user,
                    'phone_number' => 'min:10|unique:users,phone_number,'. $user,
                    'password' => 'nullable|confirmed|min:6',
                    'block' => 'bool',
                    'roles.*' => 'required|exists:roles,id',
                    'user_type' => 'numeric|in:1,2'
                ];
            default:
                break;
        }
        return [];

    }
}
