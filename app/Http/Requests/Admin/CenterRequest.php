<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class CenterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        switch ($this->method()) {
            case 'GET':
            case 'DELETE':
                return [];
            case 'POST':
                return [
                    'name' => 'required|max:255',
                    'description' => 'required|max:400',
                    'phone_number' => 'required|min:10',
                    'status' => 'required|numeric|in:1,2'
                ];
            case 'PUT':
            case 'PATCH':
                $center = $this->route()->center;
                return [
                    'name' => 'max:255' . $center,
                    'description' => 'max:400',
                    'phone_number' => 'min:10',
                    'status' => 'numeric|in:1,2'
                ];
            default:
                break;
        }
        return [];

    }
}
