<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CarRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        switch ($this->method()) {
            case 'GET':
            case 'DELETE':
                return [];
            case 'POST':
                return [
                    'name' => 'required|max:255',
                    'model' => 'required|max:400',
                    'center_id' => [
                        'required',
                        'exists:centers,id', // Check if center_id exists in the centers table
                        'numeric', // Ensure center_id is numeric
                    ],
                ];
            case 'PUT':
            case 'PATCH':
                $center = $this->route()->center;
                return [
                    'name' => 'max:255' . $center,
                    'model' => 'max:400',
                    'center_id' => [
                        'exists:centers,id', // Check if center_id exists in the centers table
                        'numeric', // Ensure center_id is numeric
                    ],                   ];
            default:
                break;
        }
        return [];

    }
}
