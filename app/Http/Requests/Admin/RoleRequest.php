<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class RoleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        switch ($this->method()) {
            case 'GET':
            case 'PATCH':
            case 'PUT':
                $role = $this->route()->role;
                return [
                    'name' => 'max:100|unique:roles,name,' . $role,
                    'display_name' => 'max:100|unique:roles,display_name,' . $role,
                    'permissions.*' => 'exists:permissions,id'
                ];
            case 'DELETE':
                return [];
            case 'POST':
                return [
                    'name' => 'required|max:100|unique:roles',
                    'display_name' => 'required|max:100|unique:roles',
                    'permissions' => 'required',
                    'permissions.*' => 'required|exists:permissions,id'
                ];
            default:
                break;
        }
        return [];
    }
}
