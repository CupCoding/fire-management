<?php


namespace App\Http\QueryFilter\Models\Dashboard;

use Illuminate\Http\Request;
use App\Http\QueryFilter\QueryFilter;

class CenterFilter extends QueryFilter
{
    protected $searchColumns = ['name', 'description'];

    public function __construct(Request $request)
    {
        $this->validationArray = [
            'name' => 'string',
            'description' => 'string',
            'search' => 'string'
        ];
        parent::__construct($request);
    }
}
