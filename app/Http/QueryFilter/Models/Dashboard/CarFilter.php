<?php


namespace App\Http\QueryFilter\Models\Dashboard;

use Illuminate\Http\Request;
use App\Http\QueryFilter\QueryFilter;

class CarFilter extends QueryFilter
{
    protected $searchColumns = ['name'];

    public function __construct(Request $request)
    {
        $this->validationArray = [
            'name' => 'string',
            'model' => 'string',
            'search' => 'string'
        ];
        parent::__construct($request);
    }
}
