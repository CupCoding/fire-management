<?php


namespace App\Http\QueryFilter\Models\Dashboard;

use Illuminate\Http\Request;
use App\Http\QueryFilter\QueryFilter;

class RoleFilter extends QueryFilter
{
    public function __construct(Request $request)
    {
        $this->validationArray = [
            'name' => 'string',
            'guard_name' => 'string',
            'search' => 'string'
        ];
        parent::__construct($request);
    }
}
