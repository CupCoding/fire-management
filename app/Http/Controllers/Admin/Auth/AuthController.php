<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Requests\Admin\LoginAdminRequest;
use App\Http\Traits\Responsible;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Laravel\Sanctum\PersonalAccessToken;

class AuthController
{
    use Responsible;

    public function login(LoginAdminRequest $request): JsonResponse
    {
        try {
            $credentials = $request->only('email', 'password');
            if (auth('api')->attempt($credentials)) {
                $admin = auth('api')->user();
                $haveTokenAdmin = false;
                $personalAccessToken = null;
                if ($admin->token) {
                    $now = Carbon::now();
                    $checkToken = explode('|', $admin->token);
                    $keyUser = $checkToken[0];
                    $personalAccessToken = $admin->tokens()->where('id', '=', $keyUser)->first();
                    $haveTokenAdmin = ($now < $personalAccessToken->expires_at);
                }

                if (!$haveTokenAdmin) {
                    $personalAccessToken?->delete();
                    $date = Carbon::now()->addMinutes(20);
                    $token = $admin->createToken('admin-Token', [User::ABILITIES['admin']], $date);
                    $admin->token = $token->plainTextToken;
                    $admin->refresh_token = hash('sha256', $token->plainTextToken);
                    $admin->save();
                }

                return $this->respondSuccess([
                    'admin' => $admin,
                    'token' => $admin->token,
                    'refresh_token' => $admin->refresh_token,
                ]);
            }
            return $this->respondError(__('auth.email_password_invalid'), 400);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function profile(): JsonResponse
    {
        return $this->respondSuccess(auth()->user());
    }

    public function refreshToken(Request $request): JsonResponse
    {
        try {
            DB::beginTransaction();

            $user = $request->user;

            $token = $user->createToken('admin-Token', [User::ABILITIES['admin']]);
            $user->token = $token->plainTextToken;
            $user->refresh_token = hash('sha256', $token->plainTextToken);

            $user->save();
            DB::commit();
            return $this->respondSuccess([
                'token' => $user->token,
                'refresh_token' => $user->refresh_token,
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

}
