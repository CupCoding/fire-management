<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\QueryFilter\Models\Dashboard\CenterFilter;
use App\Http\Requests\Admin\CenterRequest;
use App\Http\Resources\CenterResource;
use App\Http\Traits\Responsible;
use App\Models\Center;
use App\Service\CenterService;
use Illuminate\Http\JsonResponse;

class CenterController extends Controller
{
    use Responsible;

    private CenterService $centerService;

    public function __construct(CenterService $centerService)
    {
        $this->appendGeneralPermissions($this, 'center');
        $this->centerService = $centerService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param CenterFilter $brandFilter
     * @return JsonResponse
     */
    public function index(CenterFilter $brandFilter): JsonResponse
    {
        $centers = $this->centerService->findAll($brandFilter);
        return $this->respondSuccess(CenterResource::collection($centers), $centers);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CenterRequest $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function store(CenterRequest $request): JsonResponse
    {
        $center = $this->centerService->add($request->validated(), $request->file('image'));
        return $this->respondSuccess($center);
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return JsonResponse
     */
    public function show($id): JsonResponse
    {
        $center = Center::findOrFail($id);

        return $this->respondSuccess($center);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CenterRequest $request
     * @param $id
     * @return JsonResponse
     * @throws \Exception
     */
    public function update(CenterRequest $request, $id): JsonResponse
    {
        $center = Center::findOrFail($id);

        $center = $this->centerService->update($request->validated(), $center,  $request->file('image'));
        return $this->respondSuccess($center);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return JsonResponse
     * @throws \Exception
     */
    public function destroy($id): JsonResponse
    {
        $center = Center::findOrFail($id);

        $this->centerService->delete($center);

        return $this->respondSuccess($center);
    }
}
