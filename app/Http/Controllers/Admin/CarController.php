<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\QueryFilter\Models\Dashboard\CarFilter;
use App\Http\Requests\Admin\CarRequest;
use App\Http\Resources\CarResource;
use App\Http\Traits\Responsible;
use App\Models\Car;
use App\Service\CarService;
use Illuminate\Http\JsonResponse;

class CarController extends Controller
{
    use Responsible;

    private CarService $carService;

    public function __construct(CarService $carService)
    {
        $this->appendGeneralPermissions($this, 'car');
        $this->carService = $carService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param CarFilter $carFilter
     * @return JsonResponse
     */
    public function index(CarFilter $carFilter): JsonResponse
    {
        $cars = $this->carService->findAll($carFilter);
        return $this->respondSuccess(CarResource::collection($cars), $cars);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CarRequest $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function store(CarRequest $request): JsonResponse
    {
        $center = $this->carService->add($request->validated());
        return $this->respondSuccess($center);
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return JsonResponse
     */
    public function show($id): JsonResponse
    {
        $car= Car::findOrFail($id);

        return $this->respondSuccess($car);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CarRequest $request
     * @param $id
     * @return JsonResponse
     * @throws \Exception
     */
    public function update(CarRequest $request, $id): JsonResponse
    {
        $car = Car::findOrFail($id);

        $center = $this->carService->update($request->validated(), $car);
        return $this->respondSuccess($center);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return JsonResponse
     * @throws \Exception
     */
    public function destroy($id): JsonResponse
    {
        $car = Car::findOrFail($id);

        $this->carService->delete($car);

        return $this->respondSuccess($car);
    }
}
