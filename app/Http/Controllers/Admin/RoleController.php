<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\QueryFilter\Models\Dashboard\RoleFilter;
use App\Http\Requests\Admin\RoleRequest;
use App\Http\Resources\RoleResource;
use App\Http\Traits\Responsible;
use App\Models\Role;
use App\Service\RoleService;
use Illuminate\Http\JsonResponse;

class RoleController extends Controller
{
    use Responsible;

    private RoleService $roleService;

    public function __construct(RoleService $roleService)
    {
        $this->appendGeneralPermissions($this, 'role');
        $this->roleService = $roleService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(RoleFilter $roleFilter): JsonResponse
    {
        $roles = $this->roleService->findAll($roleFilter);
        return $this->respondSuccess(RoleResource::collection($roles), $roles);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param RoleRequest $request
     * @return JsonResponse
     */
    public function store(RoleRequest $request): JsonResponse
    {
        $role = $this->roleService->add($request->validated());
        return $this->respondSuccess($role);
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return JsonResponse
     */
    public function show($id): JsonResponse
    {
        $role = Role::findOrFail($id);

        return $this->respondSuccess($role);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param RoleRequest $request
     * @param $id
     * @return JsonResponse
     */
    public function update(RoleRequest $request, $id): JsonResponse
    {
        $role = Role::findOrFail($id);

        $role = $this->roleService->update($request->validated(), $role);
        return $this->respondSuccess($role);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return JsonResponse
     */
    public function destroy($id): JsonResponse
    {
        $role = Role::findOrFail($id);

        $this->roleService->delete($role);

        return $this->respondSuccess($role);
    }
}
