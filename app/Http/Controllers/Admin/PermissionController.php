<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\QueryFilter\Models\Dashboard\PermissionFilter;
use App\Http\Resources\PermissionResource;
use App\Http\Traits\Responsible;
use App\Models\Permission;
use Illuminate\Http\JsonResponse;

class PermissionController extends Controller
{
    use Responsible;

    public function __construct()
    {
        $this->middleware(['permission:permission list'], ['only' => ['index']]);
    }

    public function index(PermissionFilter $permissionFilter): JsonResponse
    {
        $permissions = Permission::query()->filter($permissionFilter);;
        return $this->respondSuccess(PermissionResource::collection($permissions), $permissions);
    }
}
