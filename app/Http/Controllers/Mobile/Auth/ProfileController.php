<?php

namespace App\Http\Controllers\Mobile\Auth;

use App\Http\Requests\Mobile\ChangePasswordRequest;
use App\Http\Requests\Mobile\ProfileRequest;
use App\Http\Traits\Responsible;
use App\Models\User;
use App\Service\UserService;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class ProfileController
{
    use Responsible;

    private UserService $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function profile(): JsonResponse
    {
        return $this->respondSuccess(auth()->user());
    }

    public function update(ProfileRequest $request): JsonResponse
    {
        $user = $this->userService->updateProfile($request->validated(), Auth::user(), $request->file('avatar'));
        return $this->respondSuccess($user);
    }

    public function changePassword(ChangePasswordRequest $request): JsonResponse
    {
        $user = $this->userService->changePassword($request->validated(), Auth::user());
        return $this->respondSuccess($user);
    }

    public function checkUser(User $user): JsonResponse {
        $user = $this->userService->checkUser($user);
        return $this->respondSuccess($user);
    }
}
