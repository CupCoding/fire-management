<?php

namespace App\Http\Controllers\Mobile\Auth;

use App\Enum\UserType;
use App\Http\Requests\Mobile\OtpLoginRequest;
use App\Http\Requests\Mobile\VerifyOtpLoginRequest;
use App\Http\Traits\Responsible;
use App\Models\User;
use App\Repositories\UserRepository;
use App\Repositories\VerificationCodeRepository;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Laravel\Sanctum\PersonalAccessToken;

class AuthController
{
    use Responsible;

    private UserRepository $userRepository;
    private VerificationCodeRepository $verificationCodeRepository;

    public function __construct(UserRepository $userRepository, VerificationCodeRepository $verificationCodeRepository)
    {
        $this->userRepository = $userRepository;
        $this->verificationCodeRepository = $verificationCodeRepository;
    }

    // Generate OTP
    public function generateOtpCode(OtpLoginRequest $request): JsonResponse
    {
        $user = $this->userRepository->findOrCreate($request->phone_number);

        $verificationCode = $this->verificationCodeRepository->findValidOne($user);
        if (!$verificationCode)
            $verificationCode = $this->verificationCodeRepository->create($user, $request->phone_number);

        //TODO send OTP code via SMS message

        return $this->respondSuccess([
            'message' => "Your OTP To Login is - " . $verificationCode->otp
        ]);

    }

    public function verifyOtpLogin(VerifyOtpLoginRequest $request): JsonResponse
    {
        try {
            $verificationCode = $this->verificationCodeRepository->findByPhoneNumber($request->phone_number);
            if (!$verificationCode || $verificationCode->otp !== $request->otp_code)
                return $this->respondError(__('auth.otp_not_correct'), 400);

            $now = Carbon::now();
            if ($now->isAfter($verificationCode->expire_at))
                return $this->respondError(__('auth.otp_expire'), 400);

            $user = $verificationCode->user;
            // Expire The OTP
            $verificationCode->update([
                'expire_at' => Carbon::now()
            ]);

            $haveTokenMobile = false;
            $personalAccessToken = null;
            if ($user->token) {
                $now = Carbon::now();
                $checkToken = explode('|', $user->token);
                $keyUser = $checkToken[0];
                $personalAccessToken = $user->tokens()->where('id', '=', $keyUser)->first();
                $haveTokenMobile = ($now < $personalAccessToken->expires_at);
                $personalAccessToken->delete();
            }

            if (!$haveTokenMobile) {
                $personalAccessToken?->delete();
                $date = Carbon::now()->addMinutes(20);
                $token = $user->createToken('customer-token', [User::ABILITIES['customer']], $date);
                $user->token = $token->plainTextToken;
                $user->refresh_token = hash('sha256', $token->plainTextToken);
                $user->user_type = UserType::CUSTOMER->value;
                $user->save();
            }

            return $this->respondSuccess([
                'user' => $user,
                'token' => $user->token,
                'refresh_token' => $user->refresh_token,
            ]);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function refreshToken(Request $request): JsonResponse
    {
        try {
            DB::beginTransaction();

            $user = $request->user;

            $token = $user->createToken('customer-token', [User::ABILITIES['customer']]);
            $user->token = $token->plainTextToken;
            $user->refresh_token = hash('sha256', $token->plainTextToken);

            $user->save();
            DB::commit();
            return $this->respondSuccess([
                'token' => $user->token,
                'refresh_token' => $user->refresh_token,
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

}
