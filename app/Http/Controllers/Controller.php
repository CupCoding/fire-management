<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    function appendGeneralPermissions(Controller $controller, $resource)
    {
        $controller->middleware(["permission:manage $resource|show $resource|update $resource|delete $resource"], ['only' => ['index', 'show']]);
        $controller->middleware(["permission:manage $resource|create $resource"], ['only' => ['store']]);
        $controller->middleware(["permission:manage $resource|update $resource"], ['only' => ['update']]);
        $controller->middleware(["permission:manage $resource|delete $resource"], ['only' => ['destroy']]);
    }
}
