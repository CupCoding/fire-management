<?php

namespace App\Models;

use App\Traits\Filterable;
use \Spatie\Permission\Models\Permission as abstractPermission;

class Permission extends abstractPermission
{
    use Filterable;

    protected $filterable = [
        'name' => 'like',
        'guard_name' => 'like'
    ];

    protected $sorts = ['id', 'name', 'guard_name', 'created_at'];

}
