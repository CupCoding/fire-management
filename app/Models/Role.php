<?php

namespace App\Models;

use App\Traits\Filterable;
use \Spatie\Permission\Models\Role as abstractRole;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Role extends abstractRole
{
    use Filterable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = ['name', 'display_name'];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
    ];

    protected $filterable = [
        'name' => 'like',
        'guard_name' => 'like'
    ];

    protected $sorts = ['id', 'name', 'guard_name', 'created_at'];

}
