<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Car extends Model
{
    protected $fillable = ['name', 'model', 'center_id'];

    public function center(): BelongsTo
    {
        return $this->belongsTo(Center::class);
    }
}
