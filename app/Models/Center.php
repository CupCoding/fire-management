<?php

namespace App\Models;

use App\Traits\Filterable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Center extends Model
{
    use HasFactory, Filterable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = ['image', 'name', 'description', 'phone_number', 'status'];

    protected $appends = ['status_name'];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
    ];

    protected $filterable = [
        'name' => 'like',
        'description' => 'like',
    ];

    protected $sorts = ['id', 'name', 'description', 'created_at'];

    public function getStatusNameAttribute()
    {
        switch ($this->status){
            case 1:
                return __( 'enum.open');
            case 2:
                return __( 'enum.close');
        }
        return $this->status;
    }

    public function cars(): HasMany
    {
        return $this->hasMany(Car::class);
    }
}
