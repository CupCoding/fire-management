<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Enum\UserType;


return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable()->index('idx_name');
            $table->string('email')->unique()->nullable()->index('idx_email');
            $table->string('password')->nullable();
            $table->string('phone_number')->unique()->index('idx_phone_number');
            $table->string('avatar')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->timestamp('phone_verified_at')->nullable();
            $table->boolean('is_active')->default(1);
            $table->tinyInteger('user_type')->default(UserType::CUSTOMER->value);
            $table->boolean('block')->default(false);
            $table->string('refresh_token', 64)->nullable()->unique();

            $table->string('token')->nullable();
            $table->rememberToken();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
